<?php

namespace App;

use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $guarded = [];
    protected $appends = ['start_date','end_date'];
    
    public function quetions()
    {
        return $this->hasMany(QuizQuetions::class, 'quiz_id', 'id');
    }
    public function cource()
    {
        return $this->hasOne(Cource::class, 'id', 'cource_id');
    }
    public function user()
    {
        return $this->hasOne(Instructor::class, 'id', 'instructor_id');
    }
    public function results()
    {
        return $this->hasMany(QuizResult::class, 'quiz_id', 'id');
    }

    public function getstartDateAttribute()
    {
        $local = app()->getLocale();
        \Date::setLocale('en');
        $date = Date::parse($this->start_at)->format("j F Y");
        return $date;
    }

    public function getendDateAttribute()
    {
        $local = app()->getLocale();
        \Date::setLocale('en');
        $date = Date::parse($this->end_at)->format("j F Y");
        return $date;
    }
}
