<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuetionBankAnswer extends Model
{
    protected $guarded = [];

    public function question()
    {
        return $this->belongsTo(QuetionBank::class, 'question_id', 'id');
    }
    
    public function instructor()
    {
        return $this->hasOne(Instructor::class, 'id', 'instructor_id');
    }
}
