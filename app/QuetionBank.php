<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuetionBank extends Model
{
    protected $guarded = [];

    public function answers()
    {
        return $this->hasMany(QuetionBankAnswer::class, 'question_id', 'id');
    }

    public function cource()
    {
        return $this->hasOne(Cource::class, 'id', 'cource_id');
    }

    public function student()
    {
        return $this->hasOne(User::class, 'id', 'student_id');
    }

    
}
