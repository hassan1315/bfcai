<?php

if (!function_exists('aurl')) {
    function aurl($url = null)
    {
        return url('admin/' . $url);
    }
}
if (!function_exists('iurl')) {
    function iurl($url = null)
    {
        return url('instructor/' . $url);
    }
}
if (!function_exists('surl')) {
    function surl($url = null)
    {
        return url(app()->getLocale() . '/student/' . $url);
    }
}

if (!function_exists('instructor')) {
    function instructor()
    {
        return auth()->guard('instructor')->user();
    }
}
if (!function_exists('student')) {
    function student()
    {
        return auth()->guard('web')->user();
    }
}

if (!function_exists('admin')) {
    function admin()
    {
        return auth()->guard('admin')->user();
    }
}

/////// Validate Helper Functions ///////
if (!function_exists('v_image')) {
    function v_image($ext = null)
    {
        if ($ext === null) {
            return 'image|mimes:jpg,png,jpeg,png,gif,bmp';
        } else {
            return 'image|mimes:' . $ext;
        }
    }
}
/////// Validate Helper Functions ///////

if (!function_exists('up')) {
    function up()
    {
        return new \App\Http\Controllers\Upload;
    }
}

/////// Ge Sahre Doctor or Assisatn in Cource ///////
if (!function_exists('share_user_id')) {
    function share_user_id($id)
    {
        if (strpos($id, '|')) {
            global $users_id ;
            $users_id = explode('|',$id);
            foreach($users_id as $user_id){
                $instructor[] = \App\Instructor::where('id', $user_id)->first()->full_name;
            }
            $instructors_name = implode(' , ',$instructor);
            return $instructors_name;
            
        } else {
            global $instructor;
            $instructor = \App\Instructor::where('id', $id)->first();
            if (!empty($instructor)) {
                return $instructor->full_name;
            } else {
                return '<span class="badge badge-danger ">Not Sahred</span>';
            }
        }        
    }
}
