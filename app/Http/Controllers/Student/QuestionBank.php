<?php

namespace App\Http\Controllers\Student;

use App\Cource;
use App\Http\Controllers\Controller;
use App\QuetionBank;
use Illuminate\Http\Request;

class QuestionBank extends Controller
{
    public function index($semester)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            $questions = QuetionBank::with('cource', 'student', 'answers')->get();
            return view('student.questionBank.index', compact('semester', 'questions'));

        } else {
            return view('student.404');
        }
    }

    public function answered($semester)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            $questions = QuetionBank::with('cource', 'student', 'answers')->get();
            return view('student.questionBank.answered', compact('semester', 'questions'));

        } else {
            return view('student.404');
        }
    }

    public function ask($semester)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            $cources = Cource::where('semester', $semester)->get();
            return view('student.questionBank.ask', compact('semester', 'cources'));

        } else {
            return view('student.404');
        }
    }

    public function ask_post($semester, Request $request)
    {
        $data = $this->validate(request(), [
            'cource_id' => ['required', 'numeric'],
            'type' => ['required', 'string', 'in:lecture,section'],
            'question' => ['required', 'string'],
        ]);

        QuetionBank::create([
            'question' => $request->question,
            'cource_id' => $request->cource_id,
            'student_id' => student()->id,
            'type' => $request->type,
        ]);

        session()->flash('success', 'Question Sent Successfuly');
        return redirect()->route('student.questionbank.index', $semester);

    }
}
