<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Quiz;
use App\QuizResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuizController extends Controller
{
    public function index($semester)
    {
        if ($semester == 'first-semester'  or $semester == 'second-semester') {

            $quizes = Quiz::with('quetions', 'cource', 'user', 'results')->get();
            return view('student.quiz.index', compact('semester', 'quizes'));

        } else {
            return view('student.404');
        }
    }

    public function details($semester, $id)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {
            $quiz = Quiz::with('quetions', 'cource', 'user', 'results')->findOrfail($id);
            return view('student.quiz.details', compact('semester', 'quiz'));

        } else {
            return view('student.404');
        }
    }

    public function solve($semester, $id)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            $quiz = Quiz::with('quetions', 'cource', 'user', 'results')->findOrfail($id);
            return view('student.quiz.solve', compact('semester', 'quiz'));
        } else {
            return view('student.404');
        }
    }
    public function result($semester, $id)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            $result = QuizResult::findOrfail($id);
            return view('student.quiz.result_page', compact('semester', 'result'));

        } else {
            return view('student.404');
        }

    }

    public function solve_post(Request $request, $semester, $id)
    {
        $quiz = Quiz::with('quetions', 'cource', 'user', 'results')->findOrfail($id);

        $validator = Validator::make($request->all(), [
            "correct_ans" => "required|array|min:1",
            "correct_ans.*" => "required",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Please Answer at least one question']);
        }

        //Define Grades varibles
        $n_question = $quiz->number_quetion;
        $total_grade = $quiz->grade;
        $qusetion_grade = $total_grade / $n_question;
        global $sudent_grade;
        $sudent_grade = array();

        //opertion to extract final grade to student
        $array_key_ans = array_keys($request->correct_ans);
        for ($i = 0; $i < max(array_keys($request->correct_ans)) + 1; $i++) {
            if (in_array($i, $array_key_ans)) {
                if (count($request->correct_ans[$i]) < 2) {
                    if (array_keys($request->correct_ans[$i]) == array_values($request->correct_ans[$i])) {
                        $student_grade[$i] = $qusetion_grade;
                    } else {
                        continue;
                    }
                } else {
                    for ($k = 0; $k < count($request->correct_ans[$i]); $k++) {
                        $correct_key = array_keys($request->correct_ans[$i]);
                        $correct_value = array_values($request->correct_ans[$i]);
                        $compine_answer = implode('|', $correct_key);
                        if ($compine_answer == $correct_value[0]) {
                            $student_grade[$i] = $qusetion_grade;
                        } else {
                            continue;
                        }
                    }
                }
            } else {
                continue;
            }
        }

        $sudent_grade = count($student_grade) * $qusetion_grade;
        $quiz_result = QuizResult::create([
            'grade' => floatval($sudent_grade),
            'quiz_id' => $quiz->id,
            'user_id' => student()->id,
        ]);

        return redirect(surl($semester . '/' . 'quiz/' . $quiz_result->id . '/result'));

    }

}
