<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index($semester, $users)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            return view('student.profile.index', compact('semester'));

        } else {
            return view('student.404');
        }

    }

    public function edit($semester,$id)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            return view('student.profile.edit',compact('semester'));

        } else {
            return view('student.404');
        }

    }
    
    public function change_password($semester,$id)
    {
    
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            return view('student.profile.changePassword',compact('semester'));

        } else {
            return view('student.404');
        }
    }

    public function grade($semester,$cource)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {

            return view('student.profile.matrial',compact('semester'));

        } else {
            return view('student.404');
        }
   
    }
}
