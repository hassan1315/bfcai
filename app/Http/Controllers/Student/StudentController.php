<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    public function index($semester)
    {
        if ($semester == 'first-semester' or $semester == 'second-semester') {
            return view('student.semester', compact('semester'));
        } else {
            return view('student.404');

        }
    }

}
