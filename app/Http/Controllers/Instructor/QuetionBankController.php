<?php

namespace App\Http\Controllers\Instructor;

use App\Http\Controllers\Controller;
use App\QuetionBank;
use App\QuetionBankAnswer;
use Illuminate\Http\Request;

class QuetionBankController extends Controller
{
    public function index($id)
    {
        $title = trans('site.QuetionBank');

        $questions = QuetionBank::where('cource_id', $id)->with('cource', 'student', 'answers')->get();
        return view('Instructor.QuetionBank.index', compact('title', 'questions', 'id'));
    }

    public function answer($id)
    {
        $title = trans('site.QuetionBank');
        $questions = QuetionBank::where('cource_id', $id)->with('cource', 'student', 'answers')->get();
        return view('Instructor.QuetionBank.answer', compact('title', 'questions', 'id'));
    }

    public function answer_post($id, Request $request)
    {

        $this->validate(request(), [
            'answer' => ['required', 'string'],
        ]);

        QuetionBankAnswer::create([
            'answer' => $request->answer,
            'question_id' => $id,
            'instructor_id' => instructor()->id,

        ]);

        session()->flash('success', 'Answered Sent Successfuly');
        return redirect()->back();

    }
    public function answer_edit($id, Request $request)
    {
   
        $this->validate(request(), [
            'answer' => ['required', 'string'],
        ]);

        QuetionBankAnswer::where('id',$id)->update(['answer' => $request->answer]);
        session()->flash('success', 'Answered Update Successfuly');
        return redirect()->back();

    }

    public function answer_delete($id)
    {
        $anss = QuetionBankAnswer::find($id);
        $anss->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->back();
    }
}
