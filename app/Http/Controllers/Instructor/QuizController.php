<?php

namespace App\Http\Controllers\Instructor;

use App\Cource;
use App\Http\Controllers\Controller;
use App\Quiz;
use App\QuizQuetions;
use Illuminate\Http\Request;

class QuizController extends Controller
{

    public function index()
    {
        $title = trans('site.view_quiz');
        return view('Instructor.quiz.view_quiz', compact('title'));
    }

    public function create()
    {
        $quiz = Quiz::create([
            'instructor_id' => instructor()->id,
        ]);
        return redirect(route("instructor.quiz.edit", $quiz->id));
    }

    public function edit($id)
    {
        $quiz = Quiz::where('id', $id)->with('quetions')->first();
        $cources = Cource::all();
        $title = trans('site.create_quiz');
        return view('Instructor.quiz.create', compact('title', 'cources', 'quiz'));
    }

    public function update($id, Request $request)
    {
        if (instructor()->type == 'doctor' or instructor()->type == 'head') {
            $request->validate([
                'name' => 'required|string',
                'type_quetion' => 'required|string',
                'cource_id' => 'required',
                'grade' => 'required|numeric',
                'time' => 'required|numeric',
                'number_quetion' => 'required|numeric',
                'start_at' => 'required',
                'end_at' => 'required',
            ]);

            $data = request()->except(['_token', '_method']);
            $data['type'] = 'lecture';

            Quiz::where('id', $id)->update($data);

            session()->flash('success', __('site.updated_successfully'));
            return redirect()->back();

        } elseif (instructor()->type == 'assistant') {
            $request->validate([
                'name' => 'required|string',
                'type_quetion' => 'required|string',
                'cource_id' => 'required',
                'grade' => 'required|numeric',
                'time' => 'required|numeric',
                'number_quetion' => 'required|numeric',
                'start_at' => 'required',
                'end_at' => 'required',
            ]);

            $data = request()->except(['_token', '_method']);
            $data['type'] = 'section';
            Quiz::where('id', $id)->update($data);

            session()->flash('success', __('site.updated_successfully'));
            return redirect()->back();

        }

    }

    public function question($id, Request $request)
    {
        $quiz = Quiz::findOrfail($id);
        if ($quiz->type_quetion == 'choices') {
            $request->validate([
                "quetions" => "required|array|min:1",
                "quetions.*" => "required|string|distinct|min:1",
                "ans_A" => "required|array|min:1",
                "ans_A.*" => "required|string|distinct|min:1",
                "ans_B" => "required|array|min:1",
                "ans_B.*" => "required|string|distinct|min:1",
                "ans_C" => "required|array|min:1",
                "ans_C.*" => "required|string|distinct|min:1",
                "ans_D" => "required|array|min:1",
                "ans_D.*" => "required|string|distinct|min:1",
                "correct_ans" => "required|array|min:1",
                "correct_ans.*" => "required|array",
            ], [], [
                "ans_A.*" => "Answer A",
                "ans_B.*" => "Answer B",
                "ans_C.*" => "Answer C",
                "ans_D.*" => "Answer D",
                "quetions.*" => "quetion",
            ]);

            QuizQuetions::where('quiz_id', $id)->delete();
            for ($i = 0; $i < count($request->quetions); $i++) {
                $corect_answers = array_keys($request->correct_ans[$i]);
                $corect_answers_string = implode('|', $corect_answers);
                QuizQuetions::create([
                    'name' => $request->quetions[$i],
                    'answer_A' => $request->ans_A[$i],
                    'answer_B' => $request->ans_B[$i],
                    'answer_C' => $request->ans_C[$i],
                    'answer_D' => $request->ans_D[$i],
                    'correct_answer' => $corect_answers_string,
                    'quiz_id' => $quiz->id,
                ]);
                $corect_answers = null;
                $corect_answers_string = null;
            }
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->back();

        } elseif ($quiz->type_quetion == 'true&false') {
            $request->validate([
                "questions" => "required|array|min:1",
                "questions.*" => "required|string|distinct|min:1",
                "correct_ans" => "required|array|min:1",
                "correct_ans.*" => "required",
            ]);

            QuizQuetions::where('quiz_id', $id)->delete();
            for ($i = 0; $i < count($request->questions); $i++) {
                if (array_key_exists($i, $request->correct_ans)) {
                    QuizQuetions::create([
                        'name' => $request->questions[$i],
                        'correct_answer' => $request->correct_ans[$i],
                        'quiz_id' => $quiz->id,
                    ]);
                } else {
                    $question = QuizQuetions::create([
                        'name' => $request->questions[$i],
                        'quiz_id' => $quiz->id,
                    ]);
                }
            }
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->back();
        }
    }

    public function quizes()
    {
        $quizes = Quiz::all();

        foreach ($quizes as $quiz) {
            if (!empty($quiz->name) and !empty($quiz->cource_id)) {
                continue;
            } else {
                Quiz::find($quiz->id)->delete();
            }
        }

        $title = trans('site.my_quizes');
        $quizes = Quiz::where('instructor_id', instructor()->id)->with('cource')->get();
        return view('Instructor.quiz.view_quiz', compact('title', 'quizes'));

    }
    public function student($id)
    {
        $title = trans('site.student_quiz');
        $quizes = Quiz::where('instructor_id', instructor()->id)->with('cource')->get();
        return view('Instructor.quiz.quiz_students', compact('title', 'quizes'));

    }
    public function destroy($id)
    {
        Quiz::destroy($id);
        session()->flash('success', __('site.quiz_delete_successfully'));
        return redirect()->route('instructor.quiz.view');
    }
}
