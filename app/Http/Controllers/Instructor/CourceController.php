<?php

namespace App\Http\Controllers\Instructor;

use App\User;
use App\Cource;
use App\Instructor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CourceController extends Controller
{
    public function create($semester)
    {
        $title = trans('site.add_course');
        $doctors_name = Instructor::where('type','doctor')->orWhere('type','head')->get();
        $assistants_name = Instructor::where('type','assistant')->get();
        return view('Instructor.cources.create', compact('title','semester','doctors_name','assistants_name'));
    }

    public function edit($id)
    {
        $title = trans('site.edit_course');
        $cource = Cource::findOrfail($id);
        return view('Instructor.cources.edit', compact('title','cource'));
    }

    public function manage()
    {
        $title = trans('site.add_course');
        $cources = Cource::all();
        return view('Instructor.cources.manage', compact('title','cources'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required|string|max:255',
            'name_en' => 'required|string|max:255',
            'cource_code' => 'required|string|max:255',
            'image' => 'sometimes|nullable|' . v_image(),
            'description' => 'sometimes|nullable|string',
        ]);
        $request_data = $request->except(['_token', 'image']);

        if ($request->hasFile('image')) {
            $request_data['image'] = up()->upload([
                'file' => 'image',
                'path' => 'instructors/cources/photo',
                'upload_type' => 'single',
                'delete_file' => '',
            ]);

        }

        if($request->share_doctor_id){
            if(count($request->share_doctor_id) > 1){
                $share_id = implode('|', $request->share_doctor_id);
                $request_data['share_doctor_id'] = $share_id;
            }
            else{
                $request_data['share_doctor_id'] = $request->share_doctor_id[0];
            }
        }
        if($request->share_asstisant_id){
            if(count($request->share_asstisant_id) > 1){
                $share_id = implode('|', $request->share_asstisant_id);
                $request_data['share_asstisant_id'] = $share_id;
            }
            else{
                $request_data['share_asstisant_id'] = $request->share_asstisant_id[0];
            }
        }

        $request_data['user_id'] = Instructor()->id;
        $request_data['semester'] = $request->semester;
        Cource::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('instructor.cources.manage');
    }

    public function update(Request $request,$id)
    {
        $cource = Cource::find($id);
        $request->validate([
            'name_ar' => 'required|string|max:255',
            'name_en' => 'required|string|max:255',
            'cource_code' => 'required|string|max:255',
            'image' => 'sometimes|nullable|' . v_image(),
            'description' => 'sometimes|nullable|string',
        ]);

        if ($request->hasFile('image')) {
            $request_data['image'] = up()->upload([
                'file' => 'image',
                'path' => 'instructors/cources/photo',
                'upload_type' => 'single',
                'delete_file' => 'instructors/cources/photo/'.$cource->image,
            ]);
        }
 
        Cource::where('id',$id)->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'cource_code' => $request->cource_code,
            'image' => $request_data['image'],
            'description' => $request->description,
            'semester' => $request->semester,
        ]);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('instructor.cources.manage');
    }

    public function destroy($id)
    {
        $cource = Cource::findOrfail($id);
        Storage::delete($cource->image);
        $cource->delete();
        session()->flash('success', 'Cource delete successfully');
        return redirect()->back();
    }

}
