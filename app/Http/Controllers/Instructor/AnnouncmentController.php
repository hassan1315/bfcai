<?php

namespace App\Http\Controllers\Instructor;

use App\Announcment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnnouncmentController extends Controller
{

    public function index()
    {
        $title = trans('site.announcment');
        return view('Instructor.announcments.index', compact('title'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required|string|min:5',
            'receive_user' => 'required|in:students,instructors',
        ]);
        $user_id = instructor()->id;
        $data = $request->except(['_token']);
        $data['user_id'] = $user_id;
        Announcment::create($data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->back();
    }
    public function edit($id)
    {
        $title = trans('site.edit_announcment');
        $announcment = Announcment::find($id);
        return view('Instructor.announcments.edit', compact('title', 'announcment'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'content' => 'required|string|min:5',
        ]);
        Announcment::where('id', $id)->update([
            'content' => $request->content,
            'receive_user' => $request->receive_user,
        ]);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('instructor.announcments.show');
    }

    public function show()
    {
        $title = trans('site.show_announcment');
        $announcments = Announcment::where('receive_user', 'instructors')->get();
        return view('Instructor.announcments.show', compact('title', 'announcments'));
    }
    public function my_announcment()
    {
        $title = trans('site.show_announcment');
        $announcments = Announcment::where('user_id', instructor()->id)->get();
        return view('Instructor.announcments.my_announcment', compact('title', 'announcments'));
    }

    public function destroy($id)
    {
        Announcment::destroy($id);
        session()->flash('success', 'Announcment delete successfully');
        return redirect()->back();
    }

}
