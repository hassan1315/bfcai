<?php
return [
    /*============================= Genral ======================*/
    'add' => 'اضف',
    'edit' => 'تعديل',
    'update' => 'تحديث',
    'yes' => 'نعم',
    'no' => 'لا',
    'loading' => 'جارى التحميل',
    'added_successfully' => 'تم اضافه البيانات بنجاح',
    'updated_successfully' => 'تم تعديل البيانات بنجاح',
    'deleted_successfully' => 'تم حذف البيانات بنجاح',
    'no_data_found' => 'للاسف لا يوجد اي سجلات',
    'delete_this' => 'هل أنت موافق ع حذف :name ؟',
    'confirm_delete' => 'تاكيد الحذف',
    'created_at' => 'تم اضافته',
    'count' => 'عدد',
    'create' => 'اضافه',
    'delete' => 'حذف',
    'print' => 'طبع',
    'search' => 'بحث',
    'status' => 'الحالة',
    'instructor' => 'Instructor',
    'student' => 'Student',
    'name_user...' => 'User Name',
    /*============================= Genral ===========================*/

    /*============================= Start Sidebar =====================*/
    'home' => 'الرئيسيه',
    'my_material' => 'المواد الخاصه بى',
    'first_semester' => 'الترم الاول',
    'lecture' => 'محاضرة',
    'section' => 'سيكشن',
    'add_material' => 'اضافه مادة',
    'second_semester' => 'الترم التانى',
    'notifactions' => 'الاشعارات',
    'messages' => 'الرسايل',
    'chat_room' => 'غرفة الدردشة',
    'students' => 'الطلاب',
    'quetion_bank' => 'بنك الأسئلة',
    'announcments' => 'الإعلانات',
    'assigments_result' => 'نتيجة الواجبات',
    'quiz' => 'لاختبارات',
    'feedback' => 'الأستبيانات',
    /*============================= End Sidebar  ======================*/

    /*============================= Start Navbar  ======================*/
    'my_profile' => 'ملفى الشخصى',
    'change_password' => 'تغيير كلمه المرور',
    'log_out' => 'تسجيل الخروج',
    'arabic' => 'اللغة العربية',
    'english' => 'اللغه الانجليزية',
    'meeting' => 'اجتماع',
    'assigment_upload' => 'تم رفع واجب ',
    'see_all' => 'رؤيه الجميع',
    /*============================= End Navbar  ========================*/

    /*============================= Start profile  ======================*/
    'profile' => 'الملف الشخصى',
    'change_profile_picture' => 'تغيير صورة الملف الشخصي',
    'upload_image' => 'تحميل صورة',
    'choice_photo' => 'اختار صوره',
    'select_image' => 'تحديد صورة',
    'edit_profile_data' => 'تعديل بيانات ملفي الشخصي',
    'first_name' => 'الاسم الأول',
    'last_name' => 'اسم العائلة',
    'email' => 'البريد الإلكتروني',
    'phone' => 'هاتف',
    'save' => 'حفظ',
    'reset' => 'إعادة',
    /*============================= End profile === ============================*/

    /*============================= Start Change Password  ======================*/
    'password_modification' => 'تعديل كلمة المرور',
    'change_password' => 'تغيير كلمة المرور',
    'current_password' => 'كلمة المرور الحالية',
    'new_password' => 'كلمة مرور جديدة',
    'password_again' => 'كلمة المرور مرة أخرى',
    /*============================= End Change Password  ======================*/

    /*============================= Start Notifaction ======================*/
    'notice' => 'الاشعار',
    'user_name' => 'اسم المستخدم',
    'date' => 'التاريخ',
    'show' => 'اظهار',
    'manage' => 'اداره',
    'meeting_will_be_held' => 'الاجتماع سوف يعقد',
    '2020-11-22' => '20 مارس 2020',
    'show_meeting' => 'رؤية الاجتماع',
    /*============================= End Notifaction ======================*/

    /*============================= Start Cource  ======================*/
    'add_course' => 'إضافة مقرر',
    'course_name_ar' => 'اسم المقرر باللغة العربية',
    'course_name_en' => 'اسم المقرر باللغة الإنجليزية',
    'course_code' => 'كود المقرر ',
    'course_image' => 'صورة المقرر ',
    'course_description' => 'وصف المقرر',
    'explan_cource_description' => 'شرح وصف المقرر',
    /*============================= End Cource  ======================*/

    /*============================= Start Lecture  ======================*/
    'lectures' => 'محاضرات',
    'sections' => 'سكاشن',
    'Choice_week...' => 'أختار الاسبوع..',
    'type_lec...' => 'أختار نوع المحاضرة..',
    'document' => 'ملف',
    'video' => 'فيديو',
    'assignment' => 'واجب منزلى',
    'assignments' => 'الواجبات',
    'manage_lecture' => 'رفع المحاضرة',
    'lectures' => 'محاضرات',
    'choice_week_upload_matriel' => 'قم بأختيار الاسبوع ونوع المحاضرة',
    'please_choice_the_week_type_upload' => 'الرجاء اختيار الأسبوع ونوع المحاضره المرفعة ',
    'uploaded_materials_this_week' => 'المواد التي تم تحميلها في هذا الأسبوع',
    'choose_the_week_and_type_of_lecture_first_upload_it.' => 'اختر الأسبوع ونوع المحاضرة أولاً قبل التحميل.',
    "please_make_sure_to_choose_the_appropriate_week_and_type." => "يرجى التأكد من اختيار الأسبوع و نوع المحاضرة المناسب.", /*============================= End Lecture  ======================*/
    'not_lectures_uploaded_yet' => 'لم يتم رفع المحاضرات بعد',
    'click_her_if_you_need_to_upload' => ' انقر هنا إذا كنت تريد الرفع ',
    'show_lecture_uploaded' => 'المحاضرات التى تم رفعها',
    'all_type' => 'جميع الانواع',
    'add_lecture' => 'إضافة محاضرة',
    'add_video' => 'إضافة فيديو',
    'add_assignment' => 'إضافة واجب',
    'lecture_name' => 'اسم المحاضرة',
    'lecture_file' => 'ملف المحاضرة',
    'lecture_video' => 'فيديو المحاضرة',
    'lecture_assignment' => 'واجب المحاضرة',
    'select_file' => 'تحديد ملف',
    'select_vide' => 'اختر فيديو',
    'select_assignment' => 'حدد الواجب',
    'lecture_description' => 'وصف المحاضرة',
    'assignment_name' => 'Assignment Name',
    'video_description' => 'Video Description',
    'assignment_description' => 'Assignment Description',
    'not_videos_uploaded_yet' => 'Not Videos Uploaded Yet',
    'not_assignments_uploaded_yet' => 'Not Assignments Uploaded Yet',
    /*============================= End Lecture  ======================*/

    /*============================= Start Assignment Result  ======================*/
    'choice_week_show_assignment_result'=> 'Choice Week To Show Assignment Result',
    'please_choice_the_week'=> 'Please Choice The Week'
    /*============================= End Assignment Result  ======================*/
];
