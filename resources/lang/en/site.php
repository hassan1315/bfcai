<?php

return [
    /*============================= Genral =====================*/
    'yes' => 'yes',
    'no' => 'no',
    'statics' => 'Statistics',
    'add' => 'Add',
    'create' => 'Create',
    'read' => 'Read',
    'edit' => 'Edit',
    'update' => 'Update',
    'delete' => 'Delete',
    'search' => 'Search',
    'loading' => 'Loading',
    "delete_this" => "Are you OK with Delete: name?",
    'confirm_delete' => 'Confirm deletion',
    'added_successfully' => 'Data was added successfully',
    'updated_successfully' => 'Data successfully modified',
    'deleted_successfully' => 'Data deleted successfully',
    'no_data_found' => 'Unfortunately there are no records',
    'instructor' => 'Instructor',
    'student' => 'Student',
    'name_user...' => 'User Name',
    /*============================= Genral =====================*/

    /*============================= Start Sidebar =====================*/
    'home' => 'Home',
    'my_material' => 'My Material',
    'first_semester' => 'First Semester',
    'lecture' => 'Lecture',
    'section' => 'Section',
    'add_material' => 'Add Material',
    'second_semester' => 'Second Semester',
    'notifactions' => 'Notifactions',
    'messages' => 'Messages',
    'chat_room' => 'Chat Room',
    'students' => 'Students',
    'quetion_bank' => 'Quetion Bank',
    'announcments' => 'Announcments',
    'assigments_result' => 'Assigments Result',
    'quiz' => 'Quiz',
    'feedback' => 'Feedback',
    'send_message' => 'Send Message',
    'sent_message' => 'Sent Message',
    'receive_message' => 'Receive Message',
    'create_quiz' => 'Create Quiz',
    'view_quiz' => 'View Quiz',
    /*============================= End Sidebar  ======================*/

    /*============================= Start Navbar  ======================*/
    'my_profile' => 'My Profile',
    'change_password' => 'Change Password',
    'log_out' => 'Log out',
    'arabic' => 'Arabic',
    'english' => 'English',
    'meeting' => 'Meeting',
    'assigment_upload' => 'Assigment Upload',
    'see_all' => 'See All',
    /*============================= End Navbar  ======================*/

    /*============================= Start profile  ======================*/
    'profile' => 'Profile',
    'change_profile_picture' => 'Change Profile Picture',
    'upload_image' => 'Upload Image',
    'choice_photo' => 'Choice Photo',
    'select_image' => 'Select Image',
    'edit_profile_data' => 'Edit My Profile Data',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'save' => 'Save',
    'reset' => 'Reset',
    /*============================= End profile  ======================*/

    /*============================= Start Change Password  ======================*/
    'password_modification' => 'Password Modification',
    'change_password' => 'Change Password',
    'current_password' => 'Current Password',
    'new_password' => 'New Password',
    'password_again' => 'Password Again',
    /*============================= End Change Password  ======================*/

    /*============================= Start Notifaction ======================*/
    'notice' => 'Notice',
    'user_name' => 'User Name',
    'date' => 'Date',
    'show' => 'Show',
    'manage' => 'Manage',
    'meeting_will_be_held' => 'Meeting will be held',
    '2020-11-22' => '2020-11-22',
    'show_meeting' => 'Show Meeting',
    /*============================ End Notifaction ======================*/

    /*============================= Start Cource  ======================*/
    'add_course' => 'Add Course',
    'course_name_ar' => 'Course Name In Arabic',
    'course_name_en' => 'Course Name In English',
    'course_code' => 'Course Code',
    'course_image' => 'Course Image',
    'course_description' => 'Course Description',
    'explain_cource_description' => 'Explain the Cource Description',
    /*============================= End Cource  ======================*/

    /*============================= Start Lecture  ======================*/
    'lectures' => 'Lectures',
    'sections' => 'Sections',
    'Choice_week...' => 'Choice Week...',
    'type_lec...' => 'Choice Type Lecture...',
    'document' => 'Document',
    'video' => 'Video',
    'all_type' => 'All Type',
    'add_new' => 'Add New',
    'assignment' => 'Assignment',
    'assignments' => 'Assignments',
    'manage_lecture' => 'Manage Lecture',
    'lectures' => 'Lectures',
    'choice_week_upload_matriel' => 'Choice week to upload Matriel',
    'please_choice_the_week_type_upload' => 'Please Choice the week and type to upload',
    'uploaded_materials_this_week' => 'Uploaded materials in this week',
    'choose_the_week_and_type_of_lecture_first_upload_it.' => 'Choose the week and type of lecture first to upload it',
    'please_make_sure_to_choose_the_appropriate_week_and_type.' => 'Please make sure to choose the appropriate week and type.',
    'not_lectures_uploaded_yet' => '  Not Lectures Uploaded yet',
    'click_her_if_you_need_to_upload' => ' Click her If you need to upload',
    'show_lecture_uploaded' => 'Show Lectures are uploded',
    'add_lecture' => 'Add Lecture',
    'add_video' => 'Add Video',
    'add_assignment' => 'Add Assignment',
    'lecture_name' => 'Lecture Name',
    'video_name' => 'Video Name',
    'assignment_name' => 'Assignment Name',
    'lecture_description' => 'Lecture Description',
    'video_description' => 'Video Description',
    'assignment_description' => 'Assignment Description',
    'lecture_file' => 'Lecture File',
    'lecture_video' => 'Lecture Video',
    'lecture_assignment' => 'Lecture Assignment',
    'select_file' => 'Select File',
    'select_video' => 'Select Video',
    'select_assignment' => 'Select Assignment',
    'lecture_description' => 'lecture Description',
    'not_videos_uploaded_yet' => 'Not Videos Uploaded Yet',
    'not_assignments_uploaded_yet' => 'Not Assignments Uploaded Yet',
    /*============================= End Lecture  ======================*/

    /*============================= Start Assignment Result ======================*/
    'choice_week_show_assignment_result' => 'Choice Week To Show Assignment Result',
    'please_choice_the_week' => 'Please Choice The Week',
    'choose_the_week_and_tcource_first_show_it' => 'Choose The Week and First To Show Assignment Soultion.',
    'show_assignment_uploaded' => 'Show Uploaded Assignment.',
    'please_make_sure_to_choose_the_appropriate_week' => 'Please make sure to choose the appropriate week.',
    'show_assignment_uploaded' => 'Show Assignments Uploaded.',
    'not_assignment_uploaded_yet' => 'Not Assignment Uploaded Yet From Students.',
    /*============================= End Assignment Result ======================*/

    /*============================= Start Messages ======================*/
    'please_choice_the_type_user' => 'Please choice the user type',
    'type_user...' => 'User Type',
    'subject' => 'Subject',
    'please_choice_the_name_of_person' => 'Please choice the name of person',
    'message_successfully' => 'Message sent successfully',
    'message_delete_successfully' => 'Message delete successfully',
    'assignemnt_name' => 'Assignemnt Name',
    /*============================= End Messages ======================*/

    /*============================= Start Messages ======================*/
    'quiz_delete_successfully' => 'Quiz delete successfully',
    /*============================= End Messages ======================*/
];
