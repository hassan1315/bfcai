@extends('layouts.student.app')

@push('style')
    <style>
    .input-label {
        background-color: #03a9f4; 
        color: #fff;
        padding: 5px 7px;
        margin-bottom: -14px;
        transition: 0.3s;
    }
    .input-label:hover{
        background-color: #03a9f4; 
    }
     .lec_sec:hover{
        cursor:pointer
    }
    
    </style>
@endpush
@push('script')
    <script>
        $(function(){
            $('#file').on('change',function(){
                $('#uploadForm').submit();
            });
        })
    </script>

@endpush
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-assignment"></i>
                        Quizes
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.quiz.index',$semester)}}">
                            <i class="icon icon-list"></i>All Quizes
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lecture
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Section
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">
        <div class="animated fadeInUpShort">
            <div class="row">
                <div class="col-md-12">
                    <div class="card no-b shadow">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr class="no-b p-3">
                                            <td class="text-center"><h5>Logo</h5></td>
                                            <td><h5>Q.Name</h5></td>
                                            <td><h5>Subject</h5></td>
                                            <td><h5>Full mark</h5></td>
                                            <td><h5>Date</h5></td>
                                            <td><h5> Type & time</h5></td>
                                            <td><h5>Availability</h5></td>
                                            <td><h5>Go</h5></td>
                                        </tr>
                                    </thead>                                      
                                       
                                    <tbody> 
                                        @foreach ($quizes as $quiz)                              
                                            <tr>
                                                <td class="w-10 ">
                                                    <img src="{{asset('student/assets/img/quiz.png')}}" alt="Quiz Logo" class="pl-2">
                                                </td>
                                                <td>
                                                    <h6>{{$quiz->name}}</h6>
                                                </td>
                                                <td>
                                                    <h6></h6>
                                                    <small class="text-muted">{{$quiz->type}}</small>
                                                </td>
                                                <td>{{$quiz->grade}} marks</td>
                                                <td>
                                                    <span><i class="icon icon-data_usage" style="color: #333"></i> 5 days ago</span><br>
                                                    <span><i class="icon icon-timer" style="color: black"></i> {{$quiz->end_at}}</span>
                                                </td>
                                                
                                                <td>
                                                    <span><i class="icon icon-clipboard" style="color: #555"></i> {{ucfirst($quiz->type_quetion)}}</span><br>
                                                    <span><i class="icon icon-timer" style="color: black"></i>{{$quiz->time}} Minute</span>
                                                </td>
                                                <td><span class="badge badge-success">available</span></td>
                                                <td>
                                                    <a target="_blank" class="btn-fab btn-fab-sm btn-primary shadow text-white" href="{{route('student.quiz.details',[Request::segment(3),$quiz->id])}}"><i class="icon-call_made "></i></a>
                                                </td>
                                            </tr>
                                        @endforeach                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
