@extends('layouts.student.app')

@push('style')
    <style>
        .res-info {
            font-size: 1.3rem;
            text-align: right;
        }
        
        .res-infon {
            font-size: 1.3rem;
            font-weight: 450;
            text-align: left;
        }
        
        .fa-check-circle {
            color: #09cc09;
            font-size: 80px;
        }
        
        .fa-times-circle {
            font-size: 80px;
            color: #cc0909;
        }
    </style>
@endpush

@push('script')
        <script>
        $(document).ready(function() {
            var answerQues = parseInt($('.ans-quest').html()) / 2;
            var myGrade = parseInt($('.the-grade').html());


            if (myGrade < answerQues) {
                $('.res-message').html("Sorry, you didn't pass!");
                $('.ic-message').removeClass("fa-check-circle");
                $('.ic-message').addClass("fa-times-circle");

            } else {
                $('.res-message').html("Congratulations, you passed!");
                $('.ic-message').addClass("fa-check-circle");
                $('.ic-message').removeClass("fa-times-circle");
            }

        });
    </script>

    <script>
        var startingMinutes = 0.2;
        let time = startingMinutes * 60;
        const countdownEl = document.getElementById('countdown');

        var myTime = setInterval(updateCountdown, 1000);

        function updateCountdown() {

            const minutes = Math.floor(time / 60);
            let seconds = time % 60;
            countdownEl.innerHTML = `${minutes} : ${seconds}`;

            time--;

            if (minutes == 0 && seconds == 0) {

            }
        }
    </script>
@endpush

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-assignment"></i>
                        Quizes
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.quiz.index','first-semester')}}">
                            <i class="icon icon-list"></i>All Quizes
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lecture
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Section
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div class="container-fluid animatedParent animateOnce my-3">
        <div class="ex-result2 row mt-40 ">
            <div class="col-md-12">
                <div class="card bg-white " style="box-shadow: 1px 5px 8px 0px #EEE;">
                    <div class="card-header bg-white">
                        <h2> Thank you <i class="far fa-smile pl-1 s-16"></i></h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">

                                <div class=" text-center m-auto">

                                    <i class="fas fa-check-circle mb-3 ic-message"></i>
                                    <h1 class="res-message mb-4" style="font-weight: 450; ">Congratulations, you passed!</h1>
                                    <div style="">
                                        <div class="row mb-2">
                                            <span class="res-info col-7 ">
                                                Total Questions:
                                            </span>
                                            <span class="res-infon col-5 ans-quest">
                                                <strong>10</strong>
                                            </span>
                                        </div>

                                        <div class="mb-2 row">
                                            <span class="res-info col-7">
                                                Your Grade:
                                            </span>
                                            <span class="res-infon col-5 the-grade">
                                               <strong>{{$result->grade}} / 10</strong>
                                            </span>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer text-center bg-white p-4">
                        <a href="{{route('student.quiz.index',Request::segment(3))}}" class="bg-primary" style=" font-size:medium; cursor:pointer; font-weight:bold; padding:10px 30px; color: #F5F8FA; border-radius: 5px; border: aliceblue;">OK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
