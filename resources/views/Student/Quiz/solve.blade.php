@extends('layouts.student.app')

@push('style')
    <style>
        .input-label {
            background-color: #03a9f4; 
            color: #fff;
            padding: 5px 7px;
            margin-bottom: -14px;
            transition: 0.3s;
        }
        .input-label:hover{
            background-color: #03a9f4; 
        }
        .lec_sec:hover{
            cursor:pointer
        } 
        .card .card-header {
            display: block !important; 
        }
    </style>
@endpush
@push('script')
    <script>
        $(function(){
            $('#file').on('change',function(){
                $('#uploadForm').submit();
            });
        })
    </script>

    <script>
        $(document).ready(function() {
            $('#solve_quiz').on('click', function(e) {
                var form = $("#solve_form");
                e.preventDefault();
                form.submit();
            });
        });

        var startingMinutes = {{$quiz->time}};
        let time = startingMinutes * 60;
        const countdownEl = document.getElementById('countdown');

        var myTime = setInterval(updateCountdown, 1000);

        function updateCountdown() {

            const minutes = Math.floor(time / 60);
            let seconds = time % 60;
            countdownEl.innerHTML = `${minutes} : ${seconds}`;

            time--;

            if (minutes == 0 && seconds == 0) {
                var form = $("#solve_form");
                form.submit();
            }
        }
    </script>
@endpush
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-assignment"></i>
                        Quizes
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.quiz.index',$semester)}}">
                            <i class="icon icon-list"></i>All Quizes
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lecture
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Section
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">
        <div class="animated fadeInUpShort">
            <div class="exam row mt-40 ">
                <div class="col-md-12">
                    <div class="card bg-white " style="box-shadow: 1px 5px 8px 0px #EEE;">
                        <div class="card-header bg-white">
                            <div class="row">
                                <h3  class="col-md-9"> Quiz solving</h3>
                                <span class="col-md-3" id="countdown" style="text-align: right; font-size: 1.5rem;"></span>
                            </div>                    
                        </div>
                        <div class="card-body">
                            <div class="row">
                               
                                <label style="font-size: 19px; text-transform: uppercase; color: #444; text-decoration: underline; margin-left: 20px; margin-bottom: 20px">Answer the following questions :</label>
                                <div class="col-md-11" style="margin-left: 40px">
                                    @if ($quiz->type_quetion == 'choices')
                                        <form id="solve_form" action="{{route('student.quiz.solve_post',[Request::segment(3),$quiz->id])}}" method="post">
                                            @csrf
                                            <div class="row quiz-model">
                                                <div class="col-md-12">
                                                    @include('partials._errors')
                                                    @foreach ($quiz->quetions as $index => $question)                                                                                            
                                                        <div class="form-group" style="margin-top: -20px !important">
                                                            <h4  class="mt-4 mb-2">                 
                                                                <span>{!! $question->name !!}</span>
                                                            </h4>
                                                            <div class="form-group mt-30">
                                                                <div class="border-checkbox-section">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-xl-6 mb-4">
                                                                            <div class="answer">
                                                                                <input style="cursor: pointer" class="border-checkbox" id="correct_ans[{{$index}}" name="correct_ans[{{$index}}][A]" value="{{$question->correct_answer}}" type="checkbox" >                                             
                                                                                <label  style="font-size: 17px">
                                                                                    <span class="pr-3">A)</span>
                                                                                    {{ $question->answer_A }}
                                                                                </label>
                                                                            </div>
                                                                        </div>   
                                                                        <div class="col-md-12 col-xl-6 mb-4">                                                      
                                                                            <div class="answer">
                                                                                <input style="cursor: pointer" class="border-checkbox" id="correct_ans[{{$index}}" name="correct_ans[{{$index}}][B]" value="{{$question->correct_answer}}" type="checkbox" >                                             
                                                                                <label style="font-size: 17px">
                                                                                    <span class="pr-3">B)</span>
                                                                                {{ $question->answer_B }}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xl-6 mb-4">                                                    
                                                                            <div class="answer">
                                                                                <input style="cursor: pointer" class="border-checkbox" id="correct_ans[{{$index}}" name="correct_ans[{{$index}}][C]" value="{{$question->correct_answer}}" type="checkbox" >                                             
                                                                                <label style="font-size: 17px">
                                                                                    <span class="pr-3">C)</span>
                                                                                {{ $question->answer_C }}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xl-6 mb-4">                                               
                                                                            <div class="answer">
                                                                                <input style="cursor: pointer" class="border-checkbox" id="correct_ans[{{$index}}]" name="correct_ans[{{$index}}][D]" value="{{$question->correct_answer}}" type="checkbox" >                                             
                                                                                <label style="font-size: 17px">
                                                                                    <span class="pr-3">D)</span>
                                                                                {{ $question->answer_D }}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>                            
                                            </div> 
                                        </form>
                        
                                    @else
                                        <form id="solve_form" action="{{route('student.quiz.solve_post',[Request::segment(3),$quiz->id])}}" method="post">
                                        @csrf
                                            <div class="row quiz-model">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <h4  class="mt-4 mb-4">                 
                                                            <span> choose the correct answer ? </span>
                                                        </h4>
                                                        <div class="form-group mt-30">
                                                            <div class="border-checkbox-section">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-6 mb-4">
                                                                        <div class="answer">
                                                                            <input  type="checkbox" >
                                                                            <label  style="font-size: 17px">
                                                                                <span class="pr-3">A)</span>
                                                                                answer number A
                                                                            </label>
                                                                        </div>
                                                                    </div>                                               

                                                                    <div class="col-md-12 col-xl-6 mb-4">
                                                                    
                                                                        <div class="answer">
                                                                            <input type="checkbox">
                                                                            <label style="font-size: 17px">
                                                                                <span class="pr-3">B)</span>
                                                                                answer number B
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12 col-xl-6 mb-4">                                                    
                                                                        <div class="answer">
                                                                            <input type="checkbox">
                                                                            <label style="font-size: 17px">
                                                                                <span class="pr-3">C)</span>
                                                                                answer number C
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12 col-xl-6 mb-4">                                               
                                                                        <div class="answer">
                                                                            <input type="checkbox">
                                                                            <label style="font-size: 17px">
                                                                                <span class="pr-3">D)</span>
                                                                                answer number D
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>                            
                                            </div>
                                        </form>
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                        <div class="card-footer text-center bg-white p-4">
                            <button  id="solve_quiz" type="submit" class="btn btn-success " style=" font-size:medium; cursor:pointer; font-weight:bold; padding:10px 30px; color: #F5F8FA; border-radius: 5px">
                                Submit
                            </button> 
                        </div>
                    </div>
                </div>
            </div>
            
            {{-- 
                <div class="ex-result row mt-40 ">
                    <div class="col-md-12">
                        <div class="card bg-white " style="box-shadow: 1px 5px 8px 0px #EEE;">
                            <div class="card-header bg-white">
                                <h3> Quiz solving</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-10 offset-md-1">
                                        
                                        <div class="card text-center m-auto" style="width: 50%;">
                                            <div class="card-header">
                                                sorry time out
                                            </div>
                                            <ul class="list-group list-group-flush ">
                                                <li class="list-group-item">Answerd questions:  <span class="badge badge-primary">20</span></li>
                                                <li class="list-group-item">Right answers:  <span class="badge badge-success">14</span></li>
                                                <li class="list-group-item">Wrong answers:  <span class="badge badge-danger">6</span></li>
                                                <li class="list-group-item">your grade:  <span class="badge badge-primary">14</span></li>
                                            </ul>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer text-center bg-white p-4">
                                
                                <button class="bg-primary "
                                style=" font-size:medium;
                                cursor:pointer;
                                font-weight:bold;
                                padding:10px 30px;
                                color: #F5F8FA;
                                border-radius: 5px;
                                border: aliceblue;" >
                                OK </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ex-expire row mt-40 ">
                    <div class="col-md-12">
                        <div class="card bg-white " style="box-shadow: 1px 5px 8px 0px #EEE;">
                            <div class="card-header bg-white">
                                <h3> Quiz solving</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-10 offset-md-1">
                                        
                                        <div class="alert alert-danger" role="alert">
                                            sorry you can't see this any more
                                            </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer text-center bg-white p-4">
                                
                                <button class="bg-primary "
                                style=" font-size:medium;
                                cursor:pointer;
                                font-weight:bold;
                                padding:10px 30px;
                                color: #F5F8FA;
                                border-radius: 5px;
                                border: aliceblue;" >
                                OK </button>
                            </div>
                        </div>
                    </div>
                </div> 
            --}}
        </div>
    </div>
</div>
@endsection
