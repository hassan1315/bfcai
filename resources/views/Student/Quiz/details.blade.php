@extends('layouts.student.app')

@push('style')
    <style>
    .input-label {
        background-color: #03a9f4; 
        color: #fff;
        padding: 5px 7px;
        margin-bottom: -14px;
        transition: 0.3s;
    }
    .input-label:hover{
        background-color: #03a9f4; 
    }
     .lec_sec:hover{
        cursor:pointer
    }
    
    </style>
@endpush
@push('script')
    <script>
        $(function(){
            $('#file').on('change',function(){
                $('#uploadForm').submit();
            });
        })
    </script>
@endpush
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-assignment"></i>
                        Quizes
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.quiz.index',$semester)}}">
                            <i class="icon icon-list"></i>All Quizes
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lecture
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Section
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">
        <div class="animated fadeInUpShort">
            <div class="row mt-40">
                <div class="col-md-12">
                    <div class="card bg-white " style="box-shadow: 1px 5px 8px 0px #EEE;">
                        <div class="card-header bg-white">
                            <h3> {{$quiz->name}} details</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">                                       
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Quiz name : &nbsp;  </label><span style="font-size: medium">{{$quiz->name}}</span>                                
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Cource name : &nbsp;  </label><span style="font-size: medium">{{$quiz->cource->name_en}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Type of quiz : &nbsp; </label> <span style="font-size: medium">{{ucfirst($quiz->type)}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Type of questions : &nbsp; </label> <span style="font-size: medium">{{ucfirst($quiz->type_quetion)}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Questions : &nbsp; </label> <span style="font-size: medium">{{$quiz->number_quetion}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Time : &nbsp; </label><span style="font-size: medium"> {{$quiz->number_quetion}} Minute</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Grade : &nbsp;  </label> <span style="font-size: medium">{{$quiz->grade}} Markss</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Instructor : &nbsp;  </label> <span style="font-size: medium">{{$quiz->user->full_name}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">Start at : &nbsp; </label><span style="font-size: medium">{{$quiz->start_at}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="quiz-detailes mb-4" style="font-size:17px; color:#777">End at : &nbsp; </label><span style="font-size: medium">{{$quiz->end_at}}</span>
                                        </div>
                                        <div class="col-12">
                                            <p style="font-size: medium">
                                                <label class="quiz-detailes " style="font-size:17px; color:#777; margin-bottom: -70px !important">Quiz description : &nbsp;  </label>
                                                {!! $quiz->description !!}
                                            </p>                                           
                                        </div>                                        
                                    </div>
                                </div>
                            </div>                          
                        </div>
                        <div class="card-footer text-center bg-white p-4">
                            <a href="{{route('student.quiz.solve',[Request::segment(3),$quiz->id])}}"  class="bg-success  st-solve"   style=" font-size:medium; cursor:pointer; font-weight:bold; padding:10px 30px; color: #FFF !important; border-radius: 5px">Start</a>
                        </div>
                    </div>
                </div>            
            </div>        
        </div>
    </div>
</div>
@endsection
