@extends('layouts.student.app')

@push('style')
<style>
        .cp, .narrow .cp {
            float: none;
            flex-wrap: nowrap;
            align-items: flex-start;
            margin-right: 0;
            padding: 0 8px 0 0;
            box-sizing: content-box;
            flex-shrink: 0;
            vertical-align: top;
            cursor: pointer;
        }
        .narrow .votes,.narrow .views,.narrow .status{
            display: inline-block;
            height: 38px;
            min-width: 38px;
            margin: 0 3px 0 0;
            font-size: 15px;
            color: var(--black-400);
            padding: 7px 6px;
            text-align: center;
        }
        .form-check-input[type="checkbox"]:checked+label:before, label.btn input[type="checkbox"]:checked+label:before {
            top: 1.8rem;
            left: -5px;
            width: 12px;
            height: 1.375rem;
            border-top: 2px solid transparent;
            border-right: 2px solid #4285f4;
            border-bottom: 2px solid #4285f4;
            border-left: 2px solid transparent;
            -webkit-transform: rotate(40deg);
            transform: rotate(40deg);
            -webkit-transform-origin: 100% 100%;
            transform-origin: 100% 100%;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
        }
        .form-check-input[type="checkbox"]+label:before, .form-check-input[type="checkbox"]:not(.filled-in)+label:after, label.btn input[type="checkbox"]+label:before, label.btn input[type="checkbox"]:not(.filled-in)+label:after {
            position: absolute;
            top: 2rem;
            left: 0;
            z-index: 0;
            width: 18px;
            height: 18px;
            margin-top: 3px;
            content: "";
            border: 2px solid #8a8a8a;
            border-radius: 1px;
            -webkit-transition: .2s;
            transition: .2s;
        }

        .form-check-input[type="checkbox"]:not(.filled-in)+label:after, label.btn input[type="checkbox"]:not(.filled-in)+label:after {
            border: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }   
        .tovote{
            display: inline-block;
        }
</style>
@endpush
@push('script')
    <script>
        var counter =parseInt(document.getElementById('votedx').innerHTML) ;
        document.getElementById('voted').onclick = function() {
            var x=document.getElementById('voted');
            if (x.checked) {
                document.getElementById('votedx').innerHTML=counter+=1;   
            }
            else{
                document.getElementById('votedx').innerHTML=counter-=1;
            }
        };
   
       $(document).ready(function(){
        $('.test').on('click',function (){               
            $('.card-footer').slideToggle();
            });
        });
    </script>
@endpush


@section('content')
<div class="page has-sidebar-left height-full">
    <div class="container-fluid relative animatedParent animateOnce">

        <div class="animated fadeInUpShort">

            <div class="row mt-4">

                <div class="col-md-12">
                    <!-- The time line -->
                    <ul class="timeline">
                        <!-- timeline time label -->
                        <li class="time-label">
                            <span class="badge badge-danger r-3">
                                10 Oct. 2020
                            </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
    
                        <!-- sent email -->
    
                        <li>
                            <i class="ion icon-envelope bg-primary"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                sent you an email
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, movity
                                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                            quora plaxo ideeli hulu weebly balihoo...
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-footer text-center">
                                    <a href="view-message.html" class="btn btn-primary ">Read more</a>
                                    <a href="#modalDeletePost" data-toggle="modal" data-target="#modalDeletePost"
                                        class="btn btn-danger ">Delete</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
    
                        <!-- upload quiz -->
                        <li>
                            <i class="ion icon-assignment yellow"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                Uploaded a new quiz
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
                                            <div class="text-center mb-3">
                                                <h5><i class="fas fa-book pr-2"></i>Security</h5>
                                                <span>lecture 1,</span>
                                                <span>week 2</span>
                                            </div>
                                            <p>
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo...
                                            </p>
    
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-footer text-center">
                                    <a href="quiz-detailes.html" class="btn btn-primary " ><i
                                            class="fas fa-eye"></i> View</a>
                                    <a href="#" class="btn btn-danger" ><i
                                            class="fas fa-file-download"> </i> Download</a>
                                </div>
                            </div>
                        </li>
    
    
                        <!-- END timeline item -->
                        <!-- timeline item -->
    
                        <!-- upload assignment -->
                        <li>
                            <i class="ion fas fa-file-signature bg-danger"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                uploaded a new Assignment
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
                                            <div class="text-center mb-3">
                                                <h5><i class="fas fa-book pr-2"></i>Security</h5>
                                                <span>lecture 1,</span>
                                                <span>week 2</span>
                                            </div>
                                            <p>
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo...
                                            </p>
    
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-footer text-center">
                                    <a href="assignments.html" class="btn btn-primary  "><i
                                            class="fas fa-eye"></i> View</a>
                                    <a href="#" class="btn btn-danger" ><i
                                            class="fas fa-file-download"> </i> Download</a>
                                </div>
                            </div>
                        </li>
    
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <!-- ash question in the question bank -->
                        <li>
                            <i class="ion fas fa-atlas green"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u1.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> Hassan Elhawary</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                Asked a question in the question bank
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
    
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
    
    
    
                                            <div class="text-center mb-3">
                                                <h5><i class="fas fa-book pr-2"></i>Security</h5>
                                                <span>lecture</span>
                                            </div>
    
    
                                            <div class="Question">
                                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Distinctio nisi
                                                    nulla voluptas earum sit
                                                    qui voluptate at non consequatur adipisci dicta, odit ut eos eaque
                                                    iusto. Officia libero labore
                                                    consequatur ?</p>
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
    
                                <div class="card-footer narrow bg-white">
                                    <div class="cp text-center ">
                                        <div class="form-check tovote">
                                            <input type="checkbox" class="form-check-input voted" id="voted"
                                                style="display: none;">
                                            <label class="votes form-check-label" for="voted" style="color: #cad900;">
                                                <div class="mini-counts " style="padding: 4px;"><span title="100 votes "
                                                        id="votedx">30</span></div>
                                                <div>votes</div>
                                            </label>
                                        </div>
    
    
    
                                        <div class="status unanswered" style="color: green;">
                                            <div class="mini-counts"><span title="0 answers">0</span></div>
                                            <div>answers</div>
                                        </div>
                                        <div class="views" style="color: red;">
                                            <div class="mini-counts"><span title="200 views">200</span></div>
                                            <div>views</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
    
                        </li>
    
    
    
                        <!-- END timeline item -->
                        <!-- timeline time label -->
                        <li class="time-label">
                            <span class="badge badge-success r-3">
                               17 Oct. 2020
                            </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
    
                        <!-- upload new file -->
                        <li>
                            <i class="ion far fa-file-alt blue"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                Uploaded a new File
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
                                            <div class="text-center mb-3">
                                                <h5><i class="fas fa-book pr-2"></i>Security</h5>
                                                <span>lecture 1,</span>
                                                <span>week 2</span>
                                            </div>
                                            <p>
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo...
                                            </p>
    
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-footer text-center">
                                    <a href="matrial.html" class="btn btn-primary  "><i
                                            class="fas fa-eye"></i> View</a>
                                    <a href="#" class="btn btn-danger"><i
                                            class="fas fa-file-download"> </i> Download</a>
                                </div>
                            </div>
                        </li>
    
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <!-- answerd question -->
                        <li>
                            <i class="ion fas fa-clipboard-check green"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                Answered your quistion
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
    
    
    
                                            <div class="text-center mb-3">
                                                <h5><i class="fas fa-book pr-2"></i>Security</h5>
                                                <span>lecture</span>
                                            </div>
    
    
                                            <div class="Question">
                                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Distinctio nisi
                                                    nulla voluptas earum sit
                                                    qui
                                                    voluptate at non consequatur adipisci dicta, odit ut eos eaque iusto.
                                                    Officia libero labore
                                                    consequatur ?
                                                </p>
                                            </div>
    
    
    
                                        </div>
                                    </div>
    
                                </div>
                                <button class="btn btn-primary test ">Show answer </button>
                                <div class="card-footer narrow bg-white p-4" style="display: none;">
                                    <div class="row">
    
                                        <div class="col-md-10 offset-md-1">
    
                                            <div class="media"
                                                style="border: 1px solid rgb(160, 160, 160); border-radius: 8px; background-color: rgb(248, 248, 248);">
                                                <img class="d-flex ml-2 mt-2 height-50" src="{{asset('student/assets/img/dummy/u3.png')}}"
                                                    alt="Generic placeholder image">
                                                <div class="media-body m-3">
    
                                                    <h5 class="mt-0 mb-1 font-weight-normal">D.Hassan Elhawary</h5>
                                                    <small>Mon 9/18/2017, 9:54 PM</small>
    
                                                    <div class=" my-3 show the-post text-center">
                                                        <div>
    
                                                            <p>
                                                                Post-ironic shabby chic VHS, Marfa keytar flannel lomo
                                                                try-hard
                                                                keffiyeh cray. Actually fap
                                                                fanny
                                                                pack yr artisan trust fund. High Life dreamcatcher
                                                                church-key
                                                                gentrify.
                                                                toast vinyl, cold-pressed try-hard blog authentic keffiyeh
                                                                Helvetica lo-fi tilde
                                                                Intelligentsia.
                                                            </p>
    
                                                        </div>
    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
    
    
    
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-10 offset-md-1">
                                            <div class="cp text-center d-block ">
    
                                                <div class="votes" style="color: #cad900;">
                                                    <div class="mini-counts"><span title="100 votes">100</span></div>
                                                    <div>votes</div>
                                                </div>
                                                <div class="status unanswered" style="color: green;">
                                                    <div class="mini-counts"><span title="0 answers">0</span></div>
                                                    <div>answers</div>
                                                </div>
                                                <div class="views" style="color: red;">
                                                    <div class="mini-counts"><span title="200 views">200</span></div>
                                                    <div>views</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    
                                </div>
                            </div>
    
    
                        </li>
    
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
    
                        <!-- post an announsment -->
    
                        <li>
                            <i class="ion far fa-clone orange"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                posted a new Announcement
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
    
                                            <p>
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo...
                                            </p>
    
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-footer text-center">
                                    <a href="anouncement.html" class="btn btn-primary ">Read more</a>
                                </div>
                            </div>
                        </li>
    
                        <!-- uploaded video -->
    
                        <li>
                            <i class="ion fas fa-play-circle indigo"></i>
    
                            <div class="timeline-item card shadow">
                                <div class="card-header white">
                                    <div class="media">
                                        <div class="the-photo pl-2 ">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px  ;height: inherit;"
                                                alt="">
                                        </div>
    
                                        <div class="discription  p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h5 class="m-0"> D.Hamid Balabel</h5>
                                                <!-- <a class="pr-1" href="#">Support Team</a> -->
                                                Uploaded a new video
                                                <span class="s-12"><i class="ion icon-clock-o pr-1"></i>Mon 9/18/2017, 9:54
                                                    PM</span>
                                            </div>
                                        </div>
                                    </div>
    
    
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
                                            <div class="text-center mb-3">
                                                <h5><i class="fas fa-book pr-2"></i>Security</h5>
                                                <span>lecture 1,</span>
                                                <span>week 2</span>
                                            </div>
                                            <p>
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo...
                                            </p>
    
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-footer text-center">
                                    <a href="matrial.html" class="btn btn-primary  "><i
                                            class="far fa-play-circle"></i> Watch</a>
                                    <a href="#" class="btn btn-danger "><i
                                            class="fas fa-cloud-download-alt"> </i> Download</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
    
                        <!-- uploaded video -->
    
            
                        <!-- END timeline item -->
                        <li>
                            <i class="ion icon-clock-o bg-gray"></i>
                        </li>
                    </ul>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>
</div>
@endsection
