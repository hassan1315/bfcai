@extends('layouts.student.app')

@push('style')
    <style>
        .cp, .narrow .cp {
            float: none;
            flex-wrap: nowrap;
            align-items: flex-start;
            margin-right: 0;
            padding: 0 8px 0 0;
            box-sizing: content-box;
            flex-shrink: 0;
            vertical-align: top;
            cursor: pointer;
        }
        .narrow .votes,.narrow .views,.narrow .status{
            display: inline-block;
            height: 38px;
            min-width: 38px;
            margin: 0 3px 0 0;
            font-size: 15px;
            color: var(--black-400);
            padding: 7px 6px;
            text-align: center;
        }
        .form-check-input[type="checkbox"]:checked+label:before, label.btn input[type="checkbox"]:checked+label:before {
            top: 1.8rem;
            left: -5px;
            width: 12px;
            height: 1.375rem;
            border-top: 2px solid transparent;
            border-right: 2px solid #4285f4;
            border-bottom: 2px solid #4285f4;
            border-left: 2px solid transparent;
            -webkit-transform: rotate(40deg);
            transform: rotate(40deg);
            -webkit-transform-origin: 100% 100%;
            transform-origin: 100% 100%;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
        }
        .form-check-input[type="checkbox"]+label:before, .form-check-input[type="checkbox"]:not(.filled-in)+label:after, label.btn input[type="checkbox"]+label:before, label.btn input[type="checkbox"]:not(.filled-in)+label:after {
            position: absolute;
            top: 2rem;
            left: 0;
            z-index: 0;
            width: 18px;
            height: 18px;
            margin-top: 3px;
            content: "";
            border: 2px solid #8a8a8a;
            border-radius: 1px;
            -webkit-transition: .2s;
            transition: .2s;
        }

        .form-check-input[type="checkbox"]:not(.filled-in)+label:after, label.btn input[type="checkbox"]:not(.filled-in)+label:after {
            border: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }   
        .tovote{
            display: inline-block;
        }
    </style>
@endpush
@push('script')
    <script>
        var counter =parseInt(document.getElementById('votedx').innerHTML) ;
        document.getElementById('voted').onclick = function() {
            var x=document.getElementById('voted');
            if (x.checked) {
                document.getElementById('votedx').innerHTML=counter+=1;   
            }
            else{
                document.getElementById('votedx').innerHTML=counter-=1;
            }
        };
    </script>
@endpush
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-assignment"></i>
                        QuestionsBank
                    </h4>
                </div>
            </div>

             <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link " href="{{route('student.questionbank.ask',$semester)}}"><i class="icon icon-list"></i>Ask Question</a>
                    </li>
                    <li>
                        <a class="nav-link active" href="{{route('student.questionbank.index',$semester)}}"><i class="fas fa-atlas pr-3"></i>Question Bank</a>
                    </li>
                    <li>
                        <a class="nav-link " href="{{route('student.questionbank.answered',$semester)}}"><i class="fas fa-clipboard-check pr-3"></i>Answerd Questions</a>
                    </li>         
                </ul>
            </div>

            <div class="row mt-3">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.questionbank.index',$semester)}}">
                            <i class="icon icon-list"></i>All Questions
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lecture
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Section
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
     <div class="container-fluid animatedParent animateOnce my-3">
        <div class="animated fadeInUpShort">
            <div class=" row mt-1">
                @if (!empty($questions->first()))
                     @foreach ($questions as $question)
                        @if (empty($question->answers->first()))
                            <div class="col-md-12 mt-2">
                                <div class="card bg-white shadow " >
                                    <div class="card-header bg-white">
                                        <div class="media">
                                            <div class="the-photo pl-2 ">
                                                <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px; height: 50px" alt="">
                                            </div>

                                            <div class="discription  p-0">
                                                <div class="dis-content pl-2 pt-2">
                                                    <h5 class="m-0"> {{$question->student->full_name}}</h5>
                                                    <span class="s-12">{{$question->created_at}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-10 offset-md-1">                               
                                                <div class="text-center mb-3"  >
                                                    <h5 ><i class="fas fa-book pr-2"></i>{{$question->cource->name_en}}<span> ({{$question->type}}) </span></h5>                                              
                                                </div>
                                                
                                                <div class="Question">
                                                    {!! $question->question !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer narrow bg-white">
                                        <div class="cp text-center">
                                        <div class="form-check tovote">
                                                <input type="checkbox" class="form-check-input voted" id="voted" style="display: none;">
                                                <label class="votes form-check-label" for="voted"style="color: #cad900;">
                                                    <div class="mini-counts " style="padding: 4px;"><span title="100 votes " id="votedx">30</span></div>
                                                    <div>votes</div>
                                                </label>
                                            </div>
                                            <div class="status unanswered" style="color: green;">
                                                <div class="mini-counts"><span title="0 answers">0</span></div>
                                                <div>answers</div>
                                            </div>
                                            <div class="views" style="color: red;">
                                                <div class="mini-counts"><span title="200 views">200</span></div>
                                                <div>views</div>
                                            </div>
                                        </div>
                                    </div>                  
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="text-center p-5">
                        <br><br><br><br>
                        <i class="icon-note-important s-64 text-primary"></i>
                        <h3 class="my-3">No Question Found</h3>
                        <p>There are no qusetion for you</p>
                        <a href="{{iurl(Request::segment(3).'/questionbank')}}" class="btn btn-primary shadow btn-lg"><i class="icon-recycle mr-2 "></i>Refresh Again</a>
                    </div>
                @endif
            </div>  
        </div>
    </div>
</div>
@endsection
