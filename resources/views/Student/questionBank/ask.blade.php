@extends('layouts.student.app')

@push('style')
<link rel="stylesheet" href="{{asset('dashboard/plugins/jquery-nice-select-1.1.0/css/nice-select.css')}}">
<style>
    .nice-select{
        line-height: 30px;
        padding-left: 15px;
    }
</style>
@endpush
@push('script')
<script src="{{asset('dashboard/plugins/jquery-nice-select-1.1.0/js/jquery.nice-select.js')}}"></script>
    <script>
        $(document).ready(function() { 
            CKEDITOR.replace('editorQ', {
                height: 180,
                language: 'en'
            });
            $('.select1').niceSelect(); 
        }); 
    </script>
@endpush
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-assignment"></i>
                        QuestionsBank
                    </h4>
                </div>
            </div>

             <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.questionbank.ask',$semester)}}"><i class="icon icon-list"></i>Ask Question</a>
                    </li>
                    <li>
                        <a class="nav-link " href="{{route('student.questionbank.index',$semester)}}"><i class="fas fa-atlas pr-3"></i>Question Bank</a>
                    </li>
                    <li>
                        <a class="nav-link " href="{{route('student.questionbank.answered',$semester)}}"><i class="fas fa-clipboard-check pr-3"></i>Answerd Questions</a>
                    </li>         
                </ul>
            </div>

            <div class="row  mt-3">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a class="nav-link active" href="{{route('student.questionbank.index',$semester)}}">
                            <i class="icon icon-list"></i>All Questions
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lecture
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="lec_sec nav-link dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Section
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Security</a>
                                <a class="dropdown-item" href="#">DSS</a>
                                <a class="dropdown-item" href="#">GIS</a>
                                <a class="dropdown-item" href="#">Cloud Computing</a>
                                <a class="dropdown-item" href="#">Advanced Database</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
     <div class="container-fluid animatedParent animateOnce my-3">
        <div class="animated fadeInUpShort">
            <div class=" row mt-40 ">
                <div class="col-md-12">
                    <div class="card bg-white shadow " >
                        <div class="card-header bg-white">
                            <h3> Ask a question</h3>
                        </div>

                        <form id="ask_question" action="{{route('student.questionbank.ask_post',$semester)}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-10 offset-md-1">
                                        <div class="mb-3">
                                            <div class="input-group m-auto" style="width: 65%; ">                                                       
                                                <select class="form-control select1 wide @error('cource_id') is-invalid @enderror" name="cource_id" style="cursor:pointer"  required>
                                                    <option>Choice Subject...</option>
                                                    @if (!empty($cources->first()))
                                                        @foreach ($cources as $cource) 
                                                            <option value="{{$cource->id}}">{{$cource->name_en}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @error('cource_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="mb-4">
                                            <div class="input-group m-auto" style="width: 65%; ">
                                                <select class="form-control select1 wide  @error('type') is-invalid @enderror" name="type" style="cursor:pointer" required>
                                                    <option value=0>Choice Type...</option>
                                                    <option value="lecture">Lecture</option>
                                                    <option value="section">Section</option>
                                                </select>
                                                @error('type')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <textarea name="question" class="form-control r-0 b-0 p-t-40 editorQ @error('question') is-invalid @enderror" id="editorQ" placeholder="Write Something..." rows="13"></textarea>
                                        @error('question')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            
                            <div class="card-footer text-center bg-white p-4">
                                <button class="bg-primary " style=" font-size:medium; cursor:pointer; font-weight:bold; padding:10px 30px; color: #F5F8FA; border-radius: 5px; border: aliceblue;" >
                                    Send
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
