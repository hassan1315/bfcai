@extends('layouts.student.app')

@push('style')
<style>
    .subject-item{
    box-shadow: 0px 0px 4px #d1cccc;
    transition: .1s ease-in-out;
  }
  
  .subject-item:hover{
    box-shadow: 1px 1px 6px #aaaaaa;
  }
</style>

@endpush
@push('scrupt')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/angular.easypiechart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/easypiechart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.js"></script> --}}

@endpush
@section('content')
<div class="page has-sidebar-left">
    <div>
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <div class="pb-3">
                            <div class="image mr-3  float-left">
                                <img class="user_avatar no-b no-p" src="{{asset('student/assets/img/dummy/u13.png')}}"   alt="User Image">
                            </div>
                            <div>
                                <h6 class="p-t-10">Hassan Elhawary</h6>
                                hassan21@gmail.com
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab"
                        role="tablist">
                        <li>
                            <a class="nav-link active" href="{{route('student.profile.index',[Request::segment(3),student()->id])}}"><i class="icon icon-home2"></i>My Profile</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('student.profile.edit',[Request::segment(3),student()->id])}}" >
                                <i class="icon icon-cog"></i>Edit Profile</a>
                        </li>
                    </ul>
                </div>

            </div>
        </header>

        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
            
            
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">
            
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><i class="icon icon-user text-primary"></i><strong class="s-12">My
                                        Name</strong> <span class="float-right s-12">Hassan Elhawary</span></li>
                                <li class="list-group-item"><i class="icon icon-mail text-success"></i><strong
                                        class="s-12">Email</strong> <span class="float-right s-12">hassan@gmail.com</span></li>
                                <li class="list-group-item"><i class="icon icon-mail text-success"></i><strong
                                        class="s-12">E.Email</strong> <span class="float-right s-12">hassan@fci.bu.edu.eg</span></li>
                                <li class="list-group-item"><i class="icon icon-mobile text-primary"></i><strong
                                        class="s-12">Phone</strong> <span class="float-right s-12">01097590701</span></li>
            
                                <li class="list-group-item"><i class="icon icon-address-card-o text-warning"></i><strong
                                        class="s-12">Address</strong> <span class="float-right s-12">cairo, Egypt</span></li>
                                <li class="list-group-item"><i class="icon icon-web text-danger"></i>
                                    <strong class="s-12">Website</strong> <span
                                        class="float-right s-12">BFCAI.tech</span></li>
                                <li class="list-group-item"><i class="icon icon-person text-success"></i><strong
                                        class="s-12">Gender</strong> <span class="float-right s-12">Male</span></li>
                                <li class="list-group-item"><i class="icon icon-date_range text-muted"></i><strong
                                        class="s-12">Birth Date</strong> <span class="float-right s-12">1/1/1998</span></li>
                            </ul>
                        </div>
            
                    </div>
                    <div class="col-md-9">
            
                        <div class="row">
                            <div class="col-lg-4 ">
                                <a href="{{route('student.profile.grade',[Request::segment(3),1])}}">
                                    <div class="card my-3 no-b subject-item">
                                        <img class="card-img-top" src="{{asset('student/assets/img/demo/portfolio/p4.jpg')}}" alt="">
            
                                        <div class="card-body">
                                            <h6 class="mb-1">Information System</h6>
                                            <span>Dr/ Ehab </span>
                                        </div>
                                        <div class="p-2 b-t">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="easy-pie-chart easy-pie-chart-sm" data-percent="30"
                                                        data-options='{"lineWidth": 5, "barColor": "#ed5564","size":60}'>
                                                        <span class="percent">30</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mt-2 ml-2">
                                                        <strong>My total score in this course</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
            
                            </div>
                            <div class="col-lg-4">
                                <a href="{{route('student.profile.grade',[Request::segment(3),1])}}">
                                    <div class="card my-3 no-b subject-item">
                                        <img class="card-img-top" src="{{asset('student/assets/img/demo/portfolio/p1.jpg')}}" alt="">
            
                                        <div class="card-body">
                                            <h6 class="mb-1">Data Base</h6>
                                            <span>Dr/ Hamid</span>
                                        </div>
                                        <div class="p-2 b-t">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="easy-pie-chart easy-pie-chart-sm" data-percent="55"
                                                        data-options='{"lineWidth": 5, "barColor": "#eeff00","size":60}'>
                                                        <span class="percent">55</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mt-2 ml-2">
                                                        <strong>My total score in this course</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
            
                            </div>
            
                            <div class="col-lg-4">
                                <a href="{{route('student.profile.grade',[Request::segment(3),1])}}">
                                    <div class="card my-3 no-b subject-item">
                                        <img class="card-img-top" src="{{asset('student/assets/img/demo/portfolio/p2.jpg')}}" alt="">
            
                                        <div class="card-body">
                                            <h6 class="mb-1">Security</h6>
                                            <span>Dr/ Hassan</span>
                                        </div>
                                        <div class="p-2 b-t">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="easy-pie-chart easy-pie-chart-sm" data-percent="70"
                                                        data-options='{"lineWidth": 5, "barColor": "#a4ed44","size":60}'>
                                                        <span class="percent">70</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mt-2 ml-2">
                                                        <strong>My total score in this course</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
            
                            </div>
                            <div class="col-lg-4">
                                <a href="{{route('student.profile.grade',[Request::segment(3),1])}}">
                                    <div class="card my-3 no-b subject-item">
                                        <img class="card-img-top" src="{{asset('student/assets/img/demo/portfolio/p5.jpg')}}" alt="">
            
                                        <div class="card-body">
                                            <h6 class="mb-1">Data Structure</h6>
                                            <span>Dr/ Noha</span>
                                        </div>
                                        <div class="p-2 b-t">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="easy-pie-chart easy-pie-chart-sm" data-percent="90"
                                                        data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                                        <span class="percent">90</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mt-2 ml-2">
                                                        <strong>My total score in this course</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
            
                            </div>
                            <div class="col-lg-4 ">
                                <a href="{{route('student.profile.grade',[Request::segment(3),1])}}">
                                    <div class="card my-3 no-b subject-item">
                                        <img class="card-img-top" src="{{asset('student/assets/img/demo/portfolio/p9.jpg')}}" alt="">
            
                                        <div class="card-body">
                                            <h6 class="mb-1">Decision Support System</h6>
                                            <span>Dr/ Mohamed</span>
                                        </div>
                                        <div class="p-2 b-t">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="easy-pie-chart easy-pie-chart-sm" data-percent="100"
                                                        data-options='{"lineWidth": 5, "barColor": "green","size":60}'>
                                                        <span class="percent">100</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mt-2 ml-2">
                                                        <strong>My total score in this course</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
            
                            </div>
                        </div>
            
            
                    </div>
                </div>
                <div class="row my-3">
                    <!-- bar charts group -->
                    <div class="col-md-12">
                        <div class="card no-b p-3 my-3">
                            <p>My grades over the semester</p>
                            <div style="height: 450px">
                                <canvas data-chart="line"
                                    data-dataset="[[0, 10, 20, 30, 40, 50, 60,'70'],[0, 3, 14, 25, 36, 47, 58, 69]]"
                                    data-labels="['0','1', '2', '3', '4', '5', '6']" data-dataset-options="datasets: [{
                                                        label: 'Total Grades',
                                                        fill: !0,
                                                        lineTension: 0,
                                                        backgroundColor: 'rgba(0,172,255, 0.1)',
                                                        borderWidth: 2,
                                                        borderColor: '#00AAFF',
                                                        borderCapStyle: 'butt',
                                                        borderDash: [],
                                                        borderDashOffset: 0,
                                                        borderJoinStyle: 'miter',
                                                        pointRadius: 4,
                                                        pointBorderColor: '#00AAFF',
                                                        pointBackgroundColor: '#fff',
                                                        pointBorderWidth: 2,
                                                        pointHoverRadius: 6,
                                                        pointHoverBackgroundColor: '#fff',
                                                        pointHoverBorderColor: '#00AAFF',
                                                        pointHoverBorderWidth: 2,
                                                        data: [0, 10, 20, 30, 40, 50, 60,'70'],
                                                        spanGaps: !1
                                                    }, {
                                                        label: 'My Grades',
                                                        fill: !0,
                                                        lineTension: 0,
                                                        backgroundColor: 'rgba(163,136,227, 0.1)',
                                                        borderWidth: 2,
                                                        borderColor: '#886CE6',
                                                        pointRadius: 4,
                                                        pointBorderColor: '#886CE6',
                                                        pointBackgroundColor: '#fff',
                                                        pointBorderWidth: 2,
                                                        pointHoverRadius: 6,
                                                        pointHoverBackgroundColor: '#fff',
                                                        pointHoverBorderColor: '#886CE6',
                                                        pointHoverBorderWidth: 2,
                                                        data: [0, 3, 14, 25, 36, 47, 58, 69],
                                                        spanGaps: !1
                                                    }]" data-options="{
                                                    legend: {
                                                        display: !0,
                                                        labels: {
                                                            fontColor: '#7F8FA4',
                                                            fontFamily: 'Source Sans Pro, sans-serif',
                                                            boxRadius: 4,
                                                            usePointStyle: !0
                                                        }
                                                    },
                                                    layout: {
                                                        padding: {
                                                            left: 0,
                                                            right: 0,
                                                            top: 0,
                                                            bottom: 0
                                                        }
                                                    },
                                                    scales: {
                                                        xAxes: [{
                                                            display: !0,
                                                            ticks: {
                                                                fontSize: '11',
                                                                fontColor: '#969da5'
                                                            },
                                                            gridLines: {
                                                                color: 'rgba(0,0,0,0.0)',
                                                                zeroLineColor: 'rgba(0,0,0,0.0)'
                                                            }
                                                        }],
                                                        yAxes: [{
                                                            display: !0,
                                                            gridLines: {
                                                                color: 'rgba(223,226,229,0.45)',
                                                                zeroLineColor: 'rgba(0,0,0,0.0)'
                                                            },
                                                            ticks: {
                                                                beginAtZero: !0,
                                                                max: 100,
                                                                stepSize: 25,
                                                                fontSize: '11',
                                                                fontColor: '#969da5'
                                                            }
                                                        }]
                                                    }
                                                }">
                                </canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /bar charts group -->
            
            
                </div>
            
                
            
            </div>
        </div>

    </div>
</div>
@endsection
