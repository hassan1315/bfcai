@extends('layouts.student.app')

@section('content')

<div class="page has-sidebar-left">
    <div>
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <div class="pb-3">
                            <div class="image mr-3  float-left">
                                <img class="user_avatar no-b no-p" src="{{asset('student/assets/img/dummy/u13.png')}}" alt="User Image">
                            </div>
                            <div>
                                <h6 class="p-t-10">Hassan Elhawary</h6>
                                hassan21@gmail.com
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab"
                        role="tablist">
                        <li>
                            <a class="nav-link " href="{{route('student.profile.index',[Request::segment(3),student()->id])}}">
                                <i class="icon icon-home2"></i>My Profile</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="{{route('student.profile.edit',[Request::segment(3),student()->id])}}">
                                <i class="icon icon-cog"></i>Edit Profile
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </header>

        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
            
                <div class="card m-3 r-0 b-0 shadow">
                    <div class="card-header white">
                        <h4>Updating informations</h4>
                    </div>
                    <div class="card-body">
                        <div class="stepper sw-main sw-theme-circles" data-options='{
                            "useURLhash":true,
                            "theme":"sw-theme-circles",
                            "transitionEffect":"fade"
                            }'>
            
                            <ul class="nav step-anchor">
                                <li><a href="#step-1y">Personal</a></li>
                                <li><a href="#step-2y">Educational</a></li>
                                <li><a href="#step-3y">Experience</a></li>
                                <li><a href="#step-4y">Avatar</a></li>
                            </ul>
                            <div>
                                <div id="step-1y" class="card-body p-3">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4" class="col-form-label">Email</label>
                                                <input class="form-control form-control-lg" id="inputEmail4"
                                                    placeholder="Email User Email" type="email">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="name" class="col-form-label">Name</label>
                                                <input class="form-control form-control-lg" id="name" placeholder="Enter User Name"
                                                    type="text">
                                            </div>
                                        </div>
            
            
                                        <div class="form-group">
                                            <label for="phonenumber" class="col-form-label">Phone Number</label>
                                            <input class="form-control form-control-lg" id="phonenumber" placeholder="Phone Number"
                                                type="number">
            
                                        </div>
            
                                        <div class="form-group">
                                            <label for="inputLinks" class="col-form-label">Links</label>
                                            <textarea class="form-control-lg" id="inputLinks" placeholder="Links"></textarea>
                                        </div>
            
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputAddress" class="col-form-label">Address</label>
                                                <input class="form-control form-control-lg" id="inputAddress"
                                                    placeholder="1234 Main St" type="text">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputCity" class="col-form-label">City</label>
                                                <input class="form-control form-control-lg" id="inputCity" type="text">
                                            </div>
                                        </div>
            
            
                                        <div class="form-row">
            
                                            <div class="form-group col-md-6">
                                                <label for="birthday" class="col-form-label">Birth day</label>
                                                <input class="form-control form-control-lg" id="birthday" data-provide="datepicker"
                                                    placeholder="Birth day" type="text">
                                            </div>
            
                                            <div class="form-group col-md-4">
                                                <label for="inputGender" class="col-form-label">Gender</label>
                                                <select id="inputGender" class="form-control form-control-lg">
                                                    <option value="">Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="inputZip" class="col-form-label">Zip</label>
                                                <input class="form-control form-control-lg" id="inputZip" type="text">
                                            </div>
                                        </div>
            
                                    </form>
                                </div>
            
            
                                <div id="step-2y" class="card-body p-3">
                                    <form>
            
                                        <div class="form-group">
                                            <label for="CollegeName" class="col-form-label">College name</label>
                                            <input class="form-control form-control-lg" id="CollegeName" placeholder="College name"
                                                type="text">
            
                                        </div>
            
                                        <div class="form-group">
                                            <label for="UniversityName" class="col-form-label">University name</label>
                                            <input class="form-control form-control-lg" id="UniversityName"
                                                placeholder="University name" type="text">
            
                                        </div>
            
                                        <div class="form-group ">
                                            <label for="inputEmailU" class="col-form-label">Email</label>
                                            <input class="form-control form-control-lg" id="inputEmailU"
                                                placeholder="your University Email" type="email">
                                        </div>
            
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <label for="UniversityAddress" class="col-form-label">University address</label>
                                                <input class="form-control form-control-lg" id="UniversityAddress"
                                                    placeholder="University address" type="email">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="inputZipU" class="col-form-label">Zip</label>
                                                <input class="form-control form-control-lg" id="inputZipU" type="text">
                                            </div>
            
                                        </div>
            
            
            
                                    </form>
                                </div>
            
            
                                <div id="step-3y" class="card-body p-3">
                                    <form>
                                        <div class="form-group">
                                            <label for="Track" class="col-form-label">Track</label>
                                            <input class="form-control form-control-lg" id="Track" placeholder="Track" type="text">
            
                                        </div>
            
                                        <div class="form-group">
                                            <label for="Mskills" class="col-form-label">Skills</label>
                                            <input class="form-control form-control-lg" id="Mskills" placeholder="Skills"
                                                type="text">
            
                                        </div>
            
                                        <div class="form-group">
                                            <label for="exLinks" class="col-form-label">Links</label>
                                            <textarea class="form-control form-control-lg" id="exLinks"
                                                placeholder="Links"></textarea>
            
                                        </div>
            
                                    </form>
                                </div>
            
            
                                <div id="step-4y" class="card-body  p-3">
                                    
                                    <div class="text-right"><button type="submit" class="btn btn-primary">Update info</button></div>
            
            
                                </div>
                            </div>
                        </div>
            
                    </div>
                </div>
            
    
            </div>
        </div>

    </div>
</div>
@endsection

