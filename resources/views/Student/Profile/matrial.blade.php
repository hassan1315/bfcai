@extends('layouts.student.app')

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <div class="pb-3">
                        <div class="image mr-3  float-left">
                            <img class="user_avatar no-b no-p" src="{{asset('student/assets/img/dummy/u13.png')}}"   alt="User Image">
                        </div>
                        <div>
                            <h6 class="p-t-10">Hassan Elhawary</h6>
                            hassan21@gmail.com
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab"
                    role="tablist">
                    <li>
                        <a class="nav-link active" href="{{route('student.profile.grade',[Request::segment(3),1])}}"><i class="icon icon-home2"></i>My Grade (secruity)</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{route('student.profile.index',[Request::segment(3),student()->id])}}" >
                            <i class="icon icon-cog"></i>My Profile</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{route('student.profile.edit',[Request::segment(3),student()->id])}}" >
                            <i class="icon icon-cog"></i>Edit Profile</a>
                    </li>
                </ul>
            </div>

        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card no-b shadow">
                    <div class="card-header white text-center">
                        <i class="icon-assignment blue-text "></i>
                        <strong> Assignments Statistics </strong>
                    </div>
                    <div class="card-body p-0 bg-light">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <!-- Table heading -->
                                <tbody>
                                    <tr class="no-b">
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Degree</th>
                                        <th>Manage</th>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Assignment 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-success r-5">T</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="70"
                                                    data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                                    <span class="percent">70</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Assignment 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-danger r-5">F</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="0"
                                                    data-options='{"lineWidth": 5, "barColor": "red","size":60}'>
                                                    <span class="percent">0</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Assignment 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-danger r-5">F</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="0"
                                                    data-options='{"lineWidth": 5, "barColor": "red","size":60}'>
                                                    <span class="percent">0</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Assignment 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-success r-5">T</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="70"
                                                    data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                                    <span class="percent">70</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card no-b shadow">
                    <div class="card-header white text-center">
                        <i class="icon-clipboard-edit blue-text "></i>
                        <strong> Quizes Statistics </strong>
                    </div>
                    <div class="card-body p-0 bg-light">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <!-- Table heading -->
                                <tbody>
                                    <tr class="no-b">
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Degree</th>
                                        <th>Manage</th>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                           Quize 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-success r-5">T</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="70"
                                                    data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                                    <span class="percent">70</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Quize 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-danger r-5">F</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="0"
                                                    data-options='{"lineWidth": 5, "barColor": "red","size":60}'>
                                                    <span class="percent">0</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Quize 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-danger r-5">F</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="0"
                                                    data-options='{"lineWidth": 5, "barColor": "red","size":60}'>
                                                    <span class="percent">0</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Page name -->
                                        <td>
                                            Quize 1
                                        </td>
                                        <!-- Page size -->
                                        <td>11/11/2020</td>
                                        <!-- Status -->
                                        <td>
                                            <span class="badge badge-success r-5">T</span>
                                        </td>
                                        <!-- Sort -->
                                        <td>
                                            <div>
                                                <span class="easy-pie-chart easy-pie-chart-sm"
                                                    data-percent="70"
                                                    data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                                    <span class="percent">70</span>
                                                </span>
                                            </div>
                                        </td>
                                        <!-- Actions -->
                                        <td>
                                            <a class="btn-fab btn-fab-sm btn-warning shadow text-white"><i
                                                    class="icon-call_made "></i></a>
                                            <a class="btn-fab btn-fab-sm btn-danger shadow text-white"><i
                                                    class="icon-cloud_download"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
        <div class="row my-3">
            <!-- bar charts group -->
            <div class="col-md-12">
                <div class="card no-b p-3 my-3">
                    <p>My grades over this course</p>
                    <div style="height: 450px">
                        <canvas
                                data-chart="line"
                                data-dataset="[[0, 10, 20, 30, 40, 50, 60,'70'],[0, 3, 14, 25, 36, 47, 58, 69]]"
                                data-labels="['0','1', '2', '3', '4', '5', '6']"
                                data-dataset-options="datasets: [{
                    label: 'Total Grades',
                    fill: !0,
                    lineTension: 0,
                    backgroundColor: 'rgba(0,172,255, 0.1)',
                    borderWidth: 2,
                    borderColor: '#00AAFF',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0,
                    borderJoinStyle: 'miter',
                    pointRadius: 4,
                    pointBorderColor: '#00AAFF',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 2,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#00AAFF',
                    pointHoverBorderWidth: 2,
                    data: [0, 10, 20, 30, 40, 50, 60,'70'],
                    spanGaps: !1
                }, {
                    label: 'My Grades',
                    fill: !0,
                    lineTension: 0,
                    backgroundColor: 'rgba(163,136,227, 0.1)',
                    borderWidth: 2,
                    borderColor: '#886CE6',
                    pointRadius: 4,
                    pointBorderColor: '#886CE6',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 2,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#886CE6',
                    pointHoverBorderWidth: 2,
                    data: [0, 3, 14, 25, 36, 47, 58, 69],
                    spanGaps: !1
                }]"
                                data-options="{
                legend: {
                    display: !0,
                    labels: {
                        fontColor: '#7F8FA4',
                        fontFamily: 'Source Sans Pro, sans-serif',
                        boxRadius: 4,
                        usePointStyle: !0
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        display: !0,
                        ticks: {
                            fontSize: '11',
                            fontColor: '#969da5'
                        },
                        gridLines: {
                            color: 'rgba(0,0,0,0.0)',
                            zeroLineColor: 'rgba(0,0,0,0.0)'
                        }
                    }],
                    yAxes: [{
                        display: !0,
                        gridLines: {
                            color: 'rgba(223,226,229,0.45)',
                            zeroLineColor: 'rgba(0,0,0,0.0)'
                        },
                        ticks: {
                            beginAtZero: !0,
                            max: 100,
                            stepSize: 25,
                            fontSize: '11',
                            fontColor: '#969da5'
                        }
                    }]
                }
            }">
                        </canvas>
                    </div>
                </div>
            </div>
            <!-- /bar charts group -->


        </div>
    </div>
</div>
@endsection

