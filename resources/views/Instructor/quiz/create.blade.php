@extends('layouts.Instructor.app')

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css">
<style>
    .creat-question:not(:first-of-type) {
        display: none;
    }

    .quiz-model {
        display: none;
    }

    #save_quiz {
        color: #fff;
        display: inline-block;
        position: absolute;
        right: 2%;
        bottom: -50px;
        padding: 13px 30px;
        border-radius: 4px;
        border: 1px solid transparent;
        font-weight: 600;
    }
    #save_quest {
        color: #fff;
        display: inline-block;
        position: absolute;
        right: 2%;
        bottom: -50px;
        padding: 13px 30px;
        border-radius: 4px;
        border: 1px solid transparent;
        font-weight: 600;
    }

    .wizard-content .wizard>.actions {
        margin-right: 110px;
    }

    .wizard-content .wizard>.content {
        overflow: visible !important;
    }

</style>
@endpush
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
<script>
    $(function () {
        
    });
</script>
<script>
    $(document).ready(function() {
        $('.select1').niceSelect();
        $(".vertical-tab-steps").steps({
            headerTag: "h6"
            , bodyTag: "section"
            , enableAllSteps: true
            , enablePagination: true
            , enableKeyNavigation: true
            , enableFinishButton: true
            , forceMoveForward: false
            , anchorSettings: {
                anchorClickable: true
                , enableAllAnchors: true
                , markDoneStep: true
                , enableAnchorOnDoneStep: true
            }
            , transitionEffect: "fade"
            , titleTemplate: '<span class="step">#index#</span> #title#'
            , labels: {
                finish: "Finish"
                , next: "Next"
                , previous: "previous"
            , }
            , onFinished: function(event, currentIndex) {
                location.replace("http://localhost/BFCAI/public/en/instructor/quizes")
            }
        });
     
        $('.quest-selection ul li').on('click', function() {
            $(this).addClass('active').siblings().removeClass('active');
            $('.the-questions .creat-question').css('display', 'none');
            $($(this).data('class')).css('display', 'block');
        });

        $('#save_quest').on('click', function(e) {
            $('#genral_quiz').replaceWith('<div>' + $('#genral_quiz').html() +'</div>');
            var form = $("#quiz_question");
            form.submit(); 
        }); 

        $('#save_quiz').on('click', function(e) {
            var form = $("#genral_quiz");
            e.preventDefault();
            form.submit();
        });

        $("#datetime1").datetimepicker({
            useCurrent: true,
        });

        $("#datetime2").datetimepicker();
        $("#datetime2").on("dp.change", function (e) {
            $('#datetime1').data("DateTimePicker").minDate(e.date);
        });

        $("#datetime1").on("dp.change", function (e) {
            $('#datetime2').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
@endpush

@if($quiz->number_quetion)
    @for ($i = 1; $i < $quiz->number_quetion + 2; $i++)
        @push('script')
            <script>
                $(document).ready(function() {
                    CKEDITOR.replace('editor{{$i}}', {
                        extraPlugins: 'placeholder'
                        , height: 100,
                        language: 'en'
                    });
                });
            </script>
        @endpush
    @endfor
@endif

@section('content')
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-check-circle bg-blue"></i>
                <div class="d-inline">
                    <h2>Create Quiz</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{iurl('/')}}"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">Create Quiz</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row mt-40">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Create Quiz</h3>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="ik ik-minus minimize-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-body wizard-content">
                <div class="dd" data-plugin="nestable">
                    <div class="vertical-tab-steps wizard-notification wizard clearfix vertical" data-toggle="validator">
                        @include('Instructor.quiz.ajax.genral_data')
                        @include('Instructor.quiz.ajax.quetions')
                        @include('Instructor.quiz.ajax.view_quiz')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
