@extends('layouts.Instructor.app')
@push('style_ss')
<link rel="stylesheet" href="{{asset('student/assets/css/app.css')}}">
@endpush
@push('style')
<style>
    .btn.btn-secondary {
        border: 1px solid transparent !important;
        margin-bottom: -70px !important;
    }
    .dropdown-menu .dropdown-item {
        background-color: teal;
        padding: 8px 15px;
        line-height: 14px;
        border-radius: 4px;
    }   
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush
@push('script')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script>
 
         $(document).ready(function() {
            var table = $('#advanced_table2').DataTable( {
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.20/i18n/English.json"
                },
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'colvis',
                            text:   '<i class="fa fa-list"></i>Custom Coulms',
                            className: 'teal',
                        },
                        {
                            extend: 'excel',
                            text:   '<i class="fa fa-file-excel-o"></i>Excel', 
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,7]
                            },
                        },
                       
                        {
                            extend: 'pdf',
                            text:   '<i class="fa fa-file-pdf-o"></i>PDF',
                            className: 'teal',
                            extend: 'pdf',
                            message: 'Quiz 1 student sheet',
                            pageSize: 'A5',
                            download: 'open',
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,7]
                            },
                           
                        }, {
                            extend: 'csv',
                            text:   '<i class="fa fa-file"></i>CSV',
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,7]
                            },
                        },
                        {
                            extend: 'print',
                            text:   '<i class="icon-print"></i>Print',
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,7]
                            },
                        },
                        {
                            extend: 'copy',
                            text:   '<i class="icon-clipboard "></i>Copy',
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,7]
                            },
                        },    
                    ]      
                });
        });
    </script>
@endpush
@section('content')
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-users bg-blue"></i>
                <div class="d-inline">
                    <h2>My Quizes</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="cpIndex.html"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">My Quizes</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Customer overview start -->
<div class="row mt-40">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Student Table :</h3>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="ik ik-minus minimize-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-body progress-task">
                <div class="dd" data-plugin="nestable">
                    <div class="table-responsive">
                        <table id="advanced_table2" class="table  table-bordered table-striped table-hover mb-0" data-repeater-list="category-group">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Q.Name</th>
                                    <th>Course</th>
                                    <th>Start at</th>
                                    <th>End at</th>
                                    <th>Grade</th>
                                    <th>Time(M)</th>
                                    <th>N.Student</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach ($quizes as $index => $quiz) 
                                    <tr class="text-center">
                                        <td>{{$index + 1}}</td>
                                        <td>@if(!empty($quiz->name)) {{ $quiz->name }} @else Not Found @endif</td>
                                        <td>@if(!empty($quiz->cource->name_en)){{ $quiz->cource->name_en }} @else Not Found  @endif</td>
                                        <td>@if(!empty($quiz->start_date)){{$quiz->start_date}} @else Not Found  @endif</td>
                                        <td>@if(!empty($quiz->end_date)){{$quiz->end_date}} @else Not Found  @endif</td>
                                        <td>@if(!empty($quiz->grade)) {{$quiz->grade}} @else Not Found @endif</td>
                                        <td>@if(!empty($quiz->time)) {{$quiz->time}} @else Not Found @endif</td>
                                        <td>143</td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a data-toggle="tooltip" data-placement="top" title="Show all students who solved the quiz" class="btn btn-icon btn-info btn-add-Details" href='{{ route('instructor.quiz.student', $quiz->id) }}'><i class="ik ik-eye f-16 mr-10"></i></a>
                                                <a data-toggle="tooltip" data-placement="top" title="Show all students who solved the quiz" class="btn btn-icon btn-primary btn-add-Details" href='{{ route('instructor.quiz.edit', $quiz->id) }}'><i class="ik ik-edit f-16 mr-10"></i></a>
                                                <a data-toggle="modal" data-target="#deleteModal{{$quiz->id}}" class="btn btn-icon btn-danger btn-add-Delete" href='#'><i class="ik ik-trash-2 f-16 mr-10" data-toggle="tooltip" data-placement="top" title="delete"></i></a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @foreach ($quizes as $index => $quiz) 
        <div class="modal fade" id="deleteModal{{$quiz->id}}"  role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Delete quiz</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <form action="{{ route('instructor.quiz.destroy', $quiz->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <div class="modal-body">
                            Do you want delete this quiz?
                        </div>
                        <div class="modal-footer">
                            <input type="submit" id="deleteVstudy" value="Delete" class="btn btn-danger"/>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection                


