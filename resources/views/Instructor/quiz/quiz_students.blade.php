@extends('layouts.Instructor.app')
@push('style_ss')
<link rel="stylesheet" href="{{asset('student/assets/css/app.css')}}">
@endpush
@push('style')
<style>
    .btn.btn-secondary {
        border: 1px solid transparent !important;
        margin-bottom: -70px !important;
    }
    .dropdown-menu .dropdown-item {
        background-color: teal;
        padding: 8px 15px;
        line-height: 14px;
        border-radius: 4px;
    }   
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush
@push('script')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script>
 
         $(document).ready(function() {
            var table = $('#advanced_table1').DataTable( {
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.20/i18n/English.json"
                },
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'colvis',
                            text:   '<i class="fa fa-list"></i>Custom Coulms',
                            className: 'teal',
                        },
                        {
                            extend: 'excel',
                            text:   '<i class="fa fa-file-excel-o"></i>Excel', 
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            },
                        },
                       
                        {
                            extend: 'pdf',
                            text:   '<i class="fa fa-file-pdf-o"></i>PDF',
                            className: 'teal',
                            extend: 'pdf',
                            message: 'Quiz 1 student sheet',
                            pageSize: 'A5',
                            download: 'open',
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            },
                           
                        }, {
                            extend: 'csv',
                            text:   '<i class="fa fa-file"></i>CSV',
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            },
                        },
                        {
                            extend: 'print',
                            text:   '<i class="icon-print"></i>Print',
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            },
                        },
                        {
                            extend: 'copy',
                            text:   '<i class="icon-clipboard "></i>Copy',
                            className: 'teal',
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            },
                        },    
                    ]      
                });
        });
    </script>
@endpush

@section('content')
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-users bg-blue"></i>
                <div class="d-inline">
                    <h2>My Students (Quiz 1)</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="cpIndex.html"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">My Students</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Customer overview start -->
<div class="row mt-40">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Students Tables</h3>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="ik ik-minus minimize-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-body progress-task">
                <div class="dd" data-plugin="nestable">
                    <div class="table-responsive">
                        <table id="advanced_table1" class="table display  table-bordered table-striped table-hover mb-0" data-repeater-list="category-group">
                             <thead>
                                <tr>
                                    <th>NO.</th>
                                    <th>Student Name </th>
                                    <th>N.Question</th>
                                    <th>Time(M)</th>
                                    <th>Grade(10)</th>
                                    <th>Date</th>
                                    <th>Mange</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach ($quizes as $index => $quiz) 
                                    <tr class="text-center">
                                        <td>{{$index + 1}}</td>
                                        <td>Hassan Mohamed Elhawary</td>
                                        <td>40</td>
                                        <td>30</td>
                                        <td>4</td>
                                        <td>15-04-2020</td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a data-toggle="tooltip" data-placement="top" title="Show Student Profile " class="btn btn-icon btn-success btn-add-Details" href='#'><i class="ik ik-user f-16 mr-10"></i></a>
                                                <a data-toggle="tooltip" data-placement="top" title="Sent Message To user" class="btn btn-icon btn-primary btn-add-Details" href='#'><i class="ik ik-message-square f-16 mr-10"></i></a>
                                            </div>
                                        </td>
                                    </tr> 
                                    <tr class="text-center">
                                        <td>{{$index + 2}}</td>
                                        <td>Hassan Mohamed Elhawary</td>
                                        <td>40</td>
                                        <td>30</td>
                                        <td>4</td>
                                        <td>15-04-2020</td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a data-toggle="tooltip" data-placement="top" title="Show Student Profile " class="btn btn-icon btn-success btn-add-Details" href='#'><i class="ik ik-user f-16 mr-10"></i></a>
                                                <a data-toggle="tooltip" data-placement="top" title="Sent Message To user" class="btn btn-icon btn-primary btn-add-Details" href='#'><i class="ik ik-message-square f-16 mr-10"></i></a>
                                            </div>
                                        </td>
                                    </tr> 
                                    <tr class="text-center">
                                        <td>{{$index + 3}}</td>
                                        <td>Hassan Mohamed Elhawary</td>
                                        <td>40</td>
                                        <td>30</td>
                                        <td>4</td>
                                        <td>15-04-2020</td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a data-toggle="tooltip" data-placement="top" title="Show Student Profile " class="btn btn-icon btn-success btn-add-Details" href='#'><i class="ik ik-user f-16 mr-10"></i></a>
                                                <a data-toggle="tooltip" data-placement="top" title="Sent Message To user" class="btn btn-icon btn-primary btn-add-Details" href='#'><i class="ik ik-message-square f-16 mr-10"></i></a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection                


