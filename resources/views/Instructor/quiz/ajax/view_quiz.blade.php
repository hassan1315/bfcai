
@push('style')
<style>
    .head_question + p{
        font-size: 20px !important;
        font-weight: bold !important;
    }
</style>
@endpush

<h6>View Quiz</h6>
<section>
    <div class="row">
        <div class="col-md-10 offset-md-1">
        @if ($quiz->type_quetion == 'choices')  
            <div class="row quiz-model2">
                <div class="col-md-12">
                    <div class="form-group">
                        <label style="font-size: 19px; text-transform: uppercase; color: #444; text-decoration: underline;">Answer the following questions :</label>
                        <span style="display:inline-block; margin-top: 50px"></span>
                        @if (!empty($quiz->quetions->first()))
                            @foreach ($quiz->quetions as $index => $question)
                                <h5 class="head_question" style="margin-top: -33px">{!! $question->name !!}</h5>
                                <div class="form-group mt-30">
                                    <div class="border-checkbox-section">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-6 mb-4">
                                                <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                    <input class="border-checkbox" type="checkbox" id="correct_ans[{{$index}}][A]" name="correct_ans[{{$index}}][A]">
                                                    <label class="border-checkbox-label " for="1">
                                                        <div class="row">
                                                            <div class="col-2" style="padding-right: inherit;">
                                                                <span style="font-size: 17px">A)</span>
                                                            </div>
                                                            <div class="col-10" style="padding: 0;font-size: 17px; ">
                                                                {{ $question->answer_A }}
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xl-6 mb-4">
                                                <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                    <input class="border-checkbox" type="checkbox" id="correct_ans[{{$index}}][B]" name="correct_ans[{{$index}}][B]">
                                                    <label class="border-checkbox-label " for="2">
                                                        <div class="row">
                                                            <div class="col-2" style="padding-right: inherit;">
                                                                <span style="font-size: 17px">B)</span>
                                                            </div>
                                                            <div class="col-10" style="padding: 0;font-size: 17px; ">
                                                                {{ $question->answer_B }}
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xl-6  mb-4">
                                                <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                    <input class="border-checkbox" type="checkbox" id="correct_ans[{{$index}}][C]" name="correct_ans[{{$index}}][C]">
                                                    <label class="border-checkbox-label " for="3">
                                                        <div class="row">
                                                            <div class="col-2" style="padding-right: inherit;">
                                                                <span style="font-size: 17px">C)</span>
                                                            </div>
                                                            <div class="col-10" style="padding: 0;font-size: 17px; ">
                                                                {{ $question->answer_B }}
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xl-6  mb-4">
                                                <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                    <input class="border-checkbox" type="checkbox" id="correct_ans[{{$index}}][D]" name="correct_ans[{{$index}}][D]">
                                                    <label class="border-checkbox-label " for="4">
                                                        <div class="row">
                                                            <div class="col-2" style="padding-right: inherit;">
                                                                <span style="font-size: 17px">D)</span>
                                                            </div>
                                                            <div class="col-10" style="padding: 0;font-size: 17px; ">
                                                                 {{ $question->answer_C }}
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach                        
                        @else
                            <div class="col-md-12 ">
                                <h3 style="margin-top: 40px; font-size: 25px" class="text-center">Please create some questions to view it.</h3>
                            </div>
                        @endif                       
                    </div>
                </div>
            </div>
        @else
            <div class="row quiz-model2">
                <div class="col-md-12">
                    <div class="form-group">
                        <label style="font-size: 20px; text-transform: uppercase; color: #444; text-decoration: underline;">Answer the following questions :</label>
                        <span style="display:inline-block; margin-top: 50px"></span>
                        @if (!empty($quiz->quetions->first()))
                            @foreach ($quiz->quetions as $index => $question)
                                <h5 class="head_question" style="margin-top: -20px">{!! $question->name !!}</h5>
                                <div class="form-group mt-30">
                                    <div class="border-checkbox-section">
                                        <div class="row">                
                                            <div class="offset-md-3 col-md-3 col-sm-12 mb-4">              
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio"  class="form-check-input" id="correct_ans[{{$i}}][T]"  name="correct_ans[{{$index}}]" value="T" style="cursor: pointer;">
                                                        <span style="font-size: 20px; display:inline-block; margin-top: -6px">True</span>
                                                    </label>
                                                </div>                      
                                            </div>
                            
                                            <div class="col-md-3 mb-4 col-sm-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio"class="form-check-input"  id="correct_ans[{{$i}}][F]" name="correct_ans[{{$index}}]" value="F" style="cursor: pointer;">
                                                        <span style="font-size: 20px; display:inline-block; margin-top: -6px">False</span>
                                                    </label>
                                                </div>
                                            </div>                   
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        @endif
        @if($quiz->type_quetion != 'choices' and $quiz->type_quetion != 'true&false')
            <div class="col-md-12 ">
                <h1 style="margin-top: 80px; font-size: 35px" class="text-center">Please Select Type Of Quiz First.</h1>
            </div> 
        @endif
        </div>
    </div>

</section>
