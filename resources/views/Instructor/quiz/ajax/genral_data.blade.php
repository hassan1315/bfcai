<h6>General Data</h6>
<section>
    <div class="row">
        <form id="genral_quiz" action="{{route('instructor.quiz.update',$quiz->id)}}" method="post">
            @csrf
            @method('put')
            <div class="col-md-10 offset-md-1">
                <div id="genral_error"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="quiz name">Quiz Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-clipboard"></i>
                                    </div>
                                </div>
                                <input id="name" value="{{ $quiz->name }}" name="name" placeholder="Quiz Name" type="text" class="form-control  @error('name') is-invalid @enderror">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type">Type of Quiz</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-arrow-right"></i>
                                    </div>
                                </div>
                                <select class="form-control select1 wide question-sort @error('type_quetion') is-invalid @enderror" id="type" name="type_quetion" required>
                                    <option>select type</option>
                                    <option value="true&false" {{$quiz->type_quetion == 'true&false' ? 'selected' : ''}} class="mcq" data-class=".quiz-model">True & False</option>
                                    <option value="choices" {{$quiz->type_quetion == 'choices' ? 'selected' : ''}} class="tf" data-class=".quiz-model2">Choices </option>
                                </select>
                                @error('type_quetion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cource_id">Cource Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-arrow-right"></i>
                                    </div>
                                </div>
                                <select class="form-control select1 wide question-sort @error('type_quetion') is-invalid @enderror" id="cource_id" name="cource_id" required>
                                    <option>select cource</option>
                                    @foreach ($cources as $cource)
                                        <option value="{{$cource->id}}" {{$quiz->cource_id == $cource->id ? 'selected' : ''}} >{{$cource->name_en}}</option>
                                    @endforeach
                                </select>
                                @error('cource_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="grade">Quiz Grade</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-award"></i>
                                    </div>
                                </div>
                                <input value="{{ $quiz->grade}}" type="number" min="1" id="grade" name="grade" placeholder="Quiz Grade" class="form-control @error('grade') is-invalid @enderror" required>
                                @error('grade')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="time">Time of quiz by minute</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-clock"></i>
                                    </div>
                                </div>
                                <input name="time" value="{{ $quiz->time }}" placeholder="Enter time" type="number" required="required" class="form-control @error('time') is-invalid @enderror">
                                @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="grade">Number of questions</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-arrow-right"></i>
                                    </div>
                                </div>
                                <input id="number_quetion" value="{{ $quiz->number_quetion }}" type="number" name="number_quetion" placeholder="number of questions" class="form-control @error('number_quetion') is-invalid @enderror" required="">
                                @error('number_quetion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="grade">Start at</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-clock"></i>
                                    </div>
                                </div>
                                <input  autocomplete="off" id="datetime1" value="{{ $quiz->start_at}}" name="start_at" class="form-control pull-right @error('start_at') is-invalid @enderror">
                                @error('start_at')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="grade">Start at</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="ik ik-clock"></i>
                                    </div>
                                </div>
                                <input autocomplete="off" id="datetime2" value="{{ $quiz->end_at}}" name="end_at" class="form-control pull-right @error('end_at') is-invalid @enderror">
                                @error('end_at')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Quiz Description </label>
                            <textarea name="description" id="editor" >{!! $quiz->description !!}</textarea>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>

            <a href="#" id="save_quiz" class="btn btn-info btn-lg">
                <span class="load"></span>Save
            </a> 
        </form>
    </div>
</section>