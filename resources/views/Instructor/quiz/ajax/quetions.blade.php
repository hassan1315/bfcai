
@push('script')
<script>
    $('#save_question').on('click', function(e) {
        var form = $("#quiz_question");
        e.preventDefault();
        form.submit();
    });
</script>
@endpush
@push('style')
<style>
    .correct-ans{
        margin-top:0.7rem ; 
        cursor: pointer;
        margin-right: -0.2rem ;

    }

    .num-ans{
        margin-top:0.7rem ;
        margin-right:0.2rem ;
        font-size: 17px; 
        
    }
    .owl-carousel {
        direction: ltr;
    }

    .owl-carousel .owl-stage {
        direction: rtl;
    }
    .slider-nav {
        margin-bottom: 30px;
        direction: ltr;
    }
    .slider-nav .slider-dot-container {
        direction: rtl;
    }

    .slider-nav .slide-arrow:hover{
        border: 2px solid #404e67;
    }
    .slider-nav .slide-arrow:hover span,
    .slider-nav .slide-arrow:hover i
    {
        color:#404e67;
    }

    .slider-nav a.slide-arrow span {
        font-size: 15px;
    }
    .slider-nav a.slide-arrow i {
        vertical-align: middle;
    }

    .slider-nav a.slide-arrow {
        padding: 0px 12px 5px;
        margin: 0 15px;
        border: 2px solid #0091c6;
        border-radius: 30px;
        overflow: hidden;
        color: #0091c6;
        -moz-transition: all 0.2s ease-in-out;
        -o-transition: all 0.2s ease-in-out;
        -webkit-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
    }

    .slider-nav a.slide-arrow span {
        display: inline-block;
        position: relative;
        font-size: 13px;
        color:#0091c6;
        font-weight: 500;
        -moz-transition: all 0.2s ease-in-out;
        -o-transition: all 0.2s ease-in-out;
        -webkit-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
    }

    .slider-nav a.left-arrow i {
        text-align:left
    }
    .slider-nav a.right-arrow i {
        text-align:right
    }
    .slider-nav a.left-arrow:hover span {
        right: 0;
        margin: 0 0 0 10px;
    }

    .slider-nav a.right-arrow:hover span {
        right: 0;
        margin: 0 10px 0 0;
    }
</style>
@endpush
<h6>Questions</h6>
<section>
    <div class="row quiz-model2">
        <div class="col-md-10 offset-md-1">
            <div class="row the-questions">
                <div class="col-md-12">
                    @if ($quiz->type_quetion == 'choices')

                        <form id="quiz_question" action="{{route('instructor.quiz.question',$quiz->id)}}" method="post">
                            @csrf
                            @for($i = 0; $i <$quiz->number_quetion; $i++)    
                                <div class="form-group creat-question question{{$i+1}}">
                                    <label>The Question No.{{$i+1}}</label>
                                    <textarea name="quetions[]" id="editor{{$i+2}}">{!! !empty($quiz->quetions->first()) ? $quiz->quetions[$i]->name : ''!!}</textarea>
                                    @error('quetions.*')
                                        <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                         
                                    <label class="mt-4"> <i data-toggle="tooltip" data-placement="top" title="" class="ik ik-info mx-2" data-original-title="Type the choices"></i>Enter the choices</label>
                                    <div class="form-group mt-30">
                                        <div class="border-checkbox-section">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-6 mb-4">
                                                    <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                        <input class="border-checkbox" id="correct_ans[{{$i}}][A]" name="correct_ans[{{$i}}][A]" @if(!empty($quiz->quetions->first())) @if (strstr($quiz->quetions[$i]->correct_answer,'A')) checked @endif @endif type="checkbox" >                                             
                                                        <label class="border-checkbox-label " for="correct_ans[{{$i}}][A]">
                                                            <div class="row">
                                                                <div class="col-2" style="padding-right: inherit;">
                                                                    <span style="font-size: 17px">A)</span>
                                                                </div>
                                                                <div class="col-10" style="padding: 0; margin-top: -3%">
                                                                    <input name="ans_A[]" value="@if(!empty($quiz->quetions->first())){{$quiz->quetions[$i]->answer_A}}@endif" class="form-control @error('ans_A.*') is-invalid @enderror">
                                                                    @error('ans_A.*')
                                                                        <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 col-xl-6 mb-4">
                                                    <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                        <input class="border-checkbox" id="correct_ans[{{$i}}][B]" name="correct_ans[{{$i}}][B]" @if(!empty($quiz->quetions->first())) @if (strstr($quiz->quetions[$i]->correct_answer,'B')) checked @endif @endif type="checkbox" >                                             
                                                        <label class="border-checkbox-label " for="correct_ans[{{$i}}][B]">
                                                            <div class="row">
                                                                <div class="col-2" style="padding-right: inherit;">
                                                                    <span style="font-size: 17px">B)</span>
                                                                </div>
                                                                <div class="col-10" style="padding: 0; margin-top: -3%">
                                                                    <input name="ans_B[]" value="@if(!empty($quiz->quetions->first())){{$quiz->quetions[$i]->answer_B}} @endif " class="form-control @error('ans_B.*') is-invalid @enderror">
                                                                    @error('ans_B.*')
                                                                        <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 col-xl-6  mb-4">
                                                    <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                        <input class="border-checkbox" id="correct_ans[{{$i}}][C]" name="correct_ans[{{$i}}][C]" @if(!empty($quiz->quetions->first())) @if (strstr($quiz->quetions[$i]->correct_answer,'C')) checked @endif @endif type="checkbox" >                                             
                                                        <label class="border-checkbox-label " for="correct_ans[{{$i}}][C]">
                                                            <div class="row">
                                                                <div class="col-2" style="padding-right: inherit;">
                                                                    <span style="font-size: 17px">C)</span>
                                                                </div>
                                                                <div class="col-10" style="padding: 0; margin-top: -3%">
                                                                    <input name="ans_C[]" value="@if(!empty($quiz->quetions->first())){{$quiz->quetions[$i]->answer_C}} @endif " class="form-control @error('ans_C.*') is-invalid @enderror">
                                                                    @error('ans_C.*')
                                                                        <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>


                                                <div class="col-md-12 col-xl-6  mb-4">
                                                    <div class="border-checkbox-group border-checkbox-group-primary " style="display:grid">
                                                        <input class="border-checkbox" id="correct_ans[{{$i}}][D]" name="correct_ans[{{$i}}][D]" @if(!empty($quiz->quetions->first())) @if (strstr($quiz->quetions[$i]->correct_answer,'D')) checked @endif @endif type="checkbox" >                                             
                                                        <label class="border-checkbox-label " for="correct_ans[{{$i}}][D]">
                                                            <div class="row">
                                                                <div class="col-2" style="padding-right: inherit;">
                                                                    <span style="font-size: 17px">D)</span>
                                                                </div>
                                                                <div class="col-10" style="padding: 0; margin-top: -3%">
                                                                    <input name="ans_D[]" value="@if(!empty($quiz->quetions->first())){{$quiz->quetions[$i]->answer_D}} @endif " class="form-control @error('ans_D.*') is-invalid @enderror">
                                                                    @error('ans_D.*')
                                                                        <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                     <input value="Save" type ="submit" id="save_quest" style="right: -9%; bottom: -9%"  class="btn btn-info btn-lg ">  
                                    </div>       
                            @endfor                           
                        </form>
                        <nav class="quest-selection" aria-label="...">
                            <ul class="pagination m-auto" style="width: max-content;">
                                @for($i = 0; $i <$quiz->number_quetion; $i++)
                                    @if($i == 0)
                                        <li class="page-item active" data-class=".question{{$i+1}}">
                                            <a class="page-link" href="#">{{$i+1}}</a>
                                        </li>
                                    @else
                                        <li class="page-item" data-class=".question{{$i+1}}">
                                            <a class="page-link" href="#">{{$i+1}}</a>
                                        </li>
                                    @endif
                                @endfor
                                <br><br>
                            </ul>
                        </nav>
                        <br><br>
                    @else
                        <form id="quiz_question" action="{{route('instructor.quiz.question',$quiz->id)}}" method="post">
                            @csrf
                            @for($i = 0; $i <$quiz->number_quetion; $i++)      
                                <div class="form-group creat-question question{{$i+1}}">
                                    <label>The Question No.{{$i+1}}</label>
                                    <textarea name="questions[]" id="editor{{$i+2}}">{{!empty($quiz->quetions->first()) ? $quiz->quetions[$i]->name : ''}}</textarea>
                                    <label for="requriedSkilles" class="mt-4"> <i data-toggle="tooltip" data-placement="top" title="" class="ik ik-info mx-2" data-original-title="Type the choices"></i>Enter the choices</label>
                                    <div class="form-group mt-30">
                                        <div class="border-checkbox-section">
                                            <div class="row">                
                                                <div class="offset-md-3 col-md-3 col-sm-12 mb-4">              
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input type="radio" @if(!empty($quiz->quetions->first())) @if ($quiz->quetions[$i]->correct_answer == 'T') checked @endif @endif class="form-check-input" id="correct_ans[{{$i}}][T]"  name="correct_ans[{{$i}}]" value="T" style="cursor: pointer;">
                                                            <span style="font-size: 20px; display:inline-block; margin-top: -6px">True</span>
                                                        </label>
                                                    </div>                      
                                                </div>
                                
                                                <div class="col-md-3 mb-4 col-sm-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input type="radio"  @if(!empty($quiz->quetions->first())) @if ($quiz->quetions[$i]->correct_answer == 'F') checked @endif @endif class="form-check-input"  id="correct_ans[{{$i}}][F]" name="correct_ans[{{$i}}]" value="F" style="cursor: pointer;">
                                                            <span style="font-size: 20px; display:inline-block; margin-top: -6px">False</span>
                                                        </label>
                                                    </div>
                                                </div>                   
                                            </div>
                                        </div>
                                    </div>
                                    <input value="Save" type ="submit" id="save_quest" style="right: -9%; bottom: -11%"  class="btn btn-info btn-lg ">  
                                </div>
                            @endfor
                        </form>

                        <nav class="quest-selection" aria-label="...">
                            <ul class="pagination m-auto" style="width: max-content;">
                                @for($i = 0; $i <$quiz->number_quetion; $i++)
                                    @if($i == 0)
                                        <li class="page-item active" data-class=".question{{$i+1}}">
                                            <a class="page-link" href="#">{{$i+1}}</a>
                                        </li>
                                    @else
                                        <li class="page-item" data-class=".question{{$i+1}}">
                                            <a class="page-link" href="#">{{$i+1}}</a>
                                        </li>
                                    @endif
                                @endfor
                                <br><br><br>
                            </ul>
                        </nav>
                    @endif

                    @if($quiz->type_quetion != 'choices' and $quiz->type_quetion != 'true&false')
                        <div class="col-md-12 ">
                            <h1 style="margin-top: 80px; font-size: 35px" class="text-center">Please Select Type Of Quiz First.</h1>
                        </div> 
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
