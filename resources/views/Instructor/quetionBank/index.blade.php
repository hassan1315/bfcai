@extends('layouts.Instructor.app')

@push('script')
<script>
    CKEDITOR.replace('editor1', {
        extraPlugins: 'placeholder'
        , height: 220
    });

</script>

@endpush
@push('style')
    <style>
        @media (min-width: 576px) {
            .modal-dialog {
                max-width: 800px !important;
            }
        }
        .cp,
        .narrow .cp {
            float: none;

            flex-wrap: nowrap;
            align-items: flex-start;
            margin-right: 0;
            padding: 0 8px 0 0;
            box-sizing: content-box;
            flex-shrink: 0;
            vertical-align: top;
            cursor: pointer;
        }

        .narrow .votes,
        .narrow .views,
        .narrow .status {
            display: inline-block;
            height: 38px;
            min-width: 38px;
            margin: 0 3px 0 0;
            font-size: 15px;
            color: var(--black-400);
            padding: 7px 6px;
            text-align: center;

        }

        .narrow .votes {
            color: orange;
        }

        .narrow .views {
            color: green;

        }

        .narrow .status {
            color: red;
        }
    </style>
@endpush
@section('content')
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-briefcase bg-blue"></i>  
                <div class="d-inline">
                    <h2 >Questions Bank</h2>
                    <a href="{{route('instructor.quetionBank.answer',$id)}}" style="display: block"class="mt-20"><span class="btn btn-success">Answerd Quetions</span></a> 
                </div>
            </div> 
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{iurl('/')}}"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">Questions Bank</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row mt-40">
    <div class="col-md-12">
        <div class="listOfDataboxs">
            @if (!empty($questions->first()))
                @foreach ($questions as $question)
                @if (empty($question->answers->first()))
                    <div class="card">                    
                        <div class="card-header bg-white">
                           <div class="col-md-10">
                               <div class="media">
                                   <div class="the-photo">
                                       <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px; height: 50px" alt="">
                                   </div>
                                   <div class="discription p-0">
                                       <div class="dis-content pl-2 pt-2">
                                           <h6 class="m-0"> {{$question->student->full_name}}</h6>
                                           <span class="s-12">{{$question->created_at}}</span>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           
                           <div class="col-md-2" style="text-align: right;">
                                <button type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#add_answer{{$question->id}}"><i class="ik ik-send"></i>Answer</button>
                            </div>
                        </div>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10 col-12 col-sm-6 col-lg-11 offset-md-1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="countrydata icon-list-item">
                                                <i class="ik ik-book"></i>
                                                <h6 style="display: inline-block; font-family: Muli">{{$question->cource->name_en}}<span> ({{$question->type}}) </span></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Question">
                                        {!! $question->question !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer narrow ">
                            <div class="cp ">
                                <div class="votes">
                                    <div class="mini-counts"><span title="100 votes">100</span></div>
                                    <div>votes</div>
                                </div>
                                <div class="status unanswered">
                                    <div class="mini-counts"><span title="0 answers">{{ count($question->answers) }}</span></div>
                                    <div>answers</div>
                                </div>
                                <div class="views">
                                    <div class="mini-counts"><span title="200 views">200</span></div>
                                    <div>views</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="add_answer{{$question->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteModalLabel">Answer This Question : </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <form action="{{ route('instructor.quetionBank.answer_post', $question->id) }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-10 offset-md-1">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="border border-secondary font-weight-bold text-red" id="question2" style="padding: 5px;">
                                                                {!! $question->question !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                        <div class="form-group">
                                                            <label for="projectIdea">Add your answer : <i data-toggle="tooltip" data-placement="top" title="Explain the project idea" class="ik ik-info mx-2"></i></label>
                                                            <textarea placeholder="" type="text" class="form-control" required cols="10" id="editor1" name="answer" rows="10"></textarea>
                                                            @error('answer')
                                                                <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Add</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
                @endforeach
            @else
            <div class="text-center p-5">
                <br><br><br><br>
                <i class="icon-note-important s-64 text-primary"></i>
                <h3 class="my-3">No Question Found</h3>
                <p>There are no qusetion for you</p>
                <a href="{{iurl(Request::segment(3).'/QuetionBank')}}" class="btn btn-primary shadow btn-lg"><i class="icon-recycle mr-2 "></i>Refresh Again</a>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection