@extends('layouts.Instructor.app')
@push('script')
<script>
    CKEDITOR.replace('editor1', {
        extraPlugins: 'placeholder'
        , height: 220
    });
    CKEDITOR.replace('editor2', {
        extraPlugins: 'placeholder'
        , height: 220
    });

    $( document ).ready( function (){
        $('.card-footer').hide();
        $('.test').on('click', function () {
            $(this).next('.card-footer').slideToggle();
        });
    } );    
</script>
<style>
    .card {
        margin-top: 12px !important;
    }
</style>
@endpush
@push('style')
<style>
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 800px !important;
        }
    }
</style>
@endpush
@section('content')
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-check-square bg-blue"></i>
                <div class="d-inline">
                    <h2>Answerd Quetions</h2>
                    <a href="{{route('instructor.quetionBank.index',$id)}}" style="display: block" class="mt-20"><span class="btn btn-danger">Quetions Not Answered</span></a> 
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{iurl('/')}}"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Answerd Quetions</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row mt-15">
    <div class="col-md-12">
        <div class="listOfDataboxs">
            @if (!empty($questions->first()))
                @foreach ($questions as $question)
                    @if (!empty($question->answers->first()))
                        <div class="card">
                            <div class="card-header bg-white">
                                <div class="col-md-10">
                                    <div class="media">
                                        <div class="the-photo">
                                            <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px; height: 50px" alt="">
                                        </div>
                                        <div class="discription p-0">
                                            <div class="dis-content pl-2 pt-2">
                                                <h6 class="m-0"> {{$question->student->full_name}}</h6>
                                                <span class="s-12">{{$question->created_at}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" style="text-align: right;">
                                    <button type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#add_answer{{$question->id}}"><i class="ik ik-plus"></i>New Answer</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-10 col-12 col-sm-6 col-lg-11 text-center ">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="countrydata icon-list-item">
                                                    <i class="ik ik-book"></i>
                                                    <div class="course-name" style="display: inline-block;">
                                                        Security</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                        <div class="Question">
                                            {!! $question->question !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-3 test">
                                <button class="btn btn-primary ">Show answer</button>
                            </div> 
                            <div class="card-footer">
                                @foreach ($question->answers as $answer_no_qusetion)
                                @php
                                    $answer = $answer_no_qusetion->with('question','instructor')->first();
                                @endphp
                                    <div class="media mt-4 ">          
                                        <div class="media-body">
                                            <h6 style="margin-top: -12px;" class="mb-10">{{$answer->instructor->full_name}}</h6>
                                            {!! $answer->answer !!}
                                        </div>
                                        <div class="dropdown">
                                            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-edit "></i></button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#edit_answer{{$answer->question->id}}">Edit</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deleteModal{{$answer->id}}">Delete</a>
                                            </div>
                                        </div>      
                                    </div>

                                    <!------------ Delete Answer Model ------------>
                                    <div class="modal fade" id="deleteModal{{$answer->id}}"  role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel">Delete quiz</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                                <form action="{{ route('instructor.quetionBank.answer_delete', $answer->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <div class="modal-body">
                                                        Do you want delete this answer?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" id="deleteVstudy" value="Delete" class="btn btn-danger"/>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <!------------ Edit Answer Model ------------>
                                    <div class="modal fade" id="edit_answer{{$answer->question->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel">Answer This Question : </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                                <form action="{{ route('instructor.quetionBank.answer_edit', $answer->id) }}" method="post">
                                                    @csrf
                                                    @method('put')
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-10 offset-md-1">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <div class="border border-secondary font-weight-bold text-red" id="question2" style="padding: 5px;">
                                                                                {!! $answer->question->question !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <div class="form-group">
                                                                            <label for="projectIdea">Add your answer : <i data-toggle="tooltip" data-placement="top" title="Explain the project idea" class="ik ik-info mx-2"></i></label>
                                                                            <textarea placeholder="" type="text" class="form-control" required cols="10" id="editor1" name="answer" rows="10">   {!! $answer->answer !!}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Edt</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="modal fade" id="add_answer{{$question->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteModalLabel">Answer This Question : </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <form action="{{ route('instructor.quetionBank.answer_post', $question->id) }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-10 offset-md-1">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="border border-secondary font-weight-bold text-red" id="question2" style="padding: 5px;">
                                                                {!! $question->question !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                        <div class="form-group">
                                                            <label for="projectIdea">Add your answer : <i data-toggle="tooltip" data-placement="top" title="Explain the project idea" class="ik ik-info mx-2"></i></label>
                                                            <textarea placeholder="" type="text" class="form-control" required cols="10" id="editor2" name="answer" rows="10"></textarea>
                                                            @error('answer')
                                                                <span class="invalid-feedback" style="display: inline-block; margin-top:-25px !important" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Add</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="text-center p-5">
                <br><br><br><br>
                <i class="icon-note-important s-64 text-primary"></i>
                <h3 class="my-3">No Question Found</h3>
                <p>There are no qusetion for you</p>
                <a href="{{iurl(Request::segment(3).'/QuetionBank/answer')}}" class="btn btn-primary shadow btn-lg"><i class="icon-recycle mr-2 "></i>Refresh Again</a>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection