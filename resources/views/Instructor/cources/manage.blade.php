@extends('layouts.Instructor.app')

@section('content')
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-users bg-blue"></i>
                <div class="d-inline">
                    <h2>My Matrials</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{iurl('/')}}"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">My Matrials</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Customer overview start -->
<div class="row mt-40">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>My Matrials Table :</h3>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="ik ik-minus minimize-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-body progress-task">
                <div class="dd" data-plugin="nestable">
                    <div class="table-responsive">
                        <table id="advanced_table2" class="table  table-bordered table-striped table-hover mb-0" data-repeater-list="category-group">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>C.Name En</th>
                                    <th>C.Name Ar</th>
                                    <th>Course Code</th>
                                    <th>semester</th>
                                    <th>Sahre Doctor</th>
                                    <th>Sahre Assistant</th>
                                    <th>Created at</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach ($cources as $index => $cource) 
                                    <tr class="text-center">
                                        <td>{{$index +1}}</td>
                                        <td>{{$cource->name_en}}</td>
                                        <td>{{$cource->name_ar}}</td>
                                        <td>{{$cource->cource_code}}</td>
                                        <td>{{ucfirst(str_replace('-',' ',$cource->semester))}}</td>
                                        <td>{!! share_user_id($cource->share_doctor_id)!!}</td>
                                        <td>{!! share_user_id($cource->share_asstisant_id)!!}</td> 
                                        <td>{{$cource->created_at->toDateString()}}</td>
                                        <td class="text-center">
                                            <div class="action-btns">
                                            @if (instructor()->type == 'doctor' or instructor()->type == 'head' )
                                                <a class="btn btn-icon btn-info btn-add-Details" href='{{route('instructor.cources.lectures.index',$cource->id)}}'><i class="ik ik-eye f-16 mr-10"></i></a>
                                            @elseif(instructor()->type == 'assistant')
                                                <a class="btn btn-icon btn-info btn-add-Details" href='{{route('instructor.cources.sections.index',$cource->id)}}'><i class="ik ik-eye f-16 mr-10"></i></a>
                                            @endif
                                                <a class="btn btn-icon btn-primary btn-add-edit" href='{{route('instructor.cources.edit',$cource->id)}}'><i class="ik ik-edit f-16 mr-10" title="edit"></i></a>
                                                <a data-toggle="modal" data-target="#deleteModal{{$cource->id}}" class="btn btn-icon btn-danger btn-add-Delete" href='#'><i class="ik ik-trash-2 f-16 mr-10" data-toggle="tooltip" data-placement="top" title="delete"></i></a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @foreach ($cources as $index => $cource) 
        <div class="modal fade" id="deleteModal{{$cource->id}}"  role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Delete Cource </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <form action="{{route('instructor.cources.destroy',$cource->id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <div class="modal-body">
                            Do you want delete this cource?
                        </div>
                        <div class="modal-footer">
                            <input type="submit" id="deleteVstudy" value="Delete" class="btn btn-danger"/>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection                


