@extends('layouts.Instructor.app')
@push('style')
    <link rel="stylesheet" href="{{asset('student/assets/csss/app.css')}}">
    <style>

        .cp,
        .narrow .cp {
            float: none;

            flex-wrap: nowrap;
            align-items: flex-start;
            margin-right: 0;
            padding: 0 8px 0 0;
            box-sizing: content-box;
            flex-shrink: 0;
            vertical-align: top;
            cursor: pointer;
        }
    </style>
@endpush
@section('content')

<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-list bg-blue"></i>
                <div class="d-inline">
                    <h2>My Announcement</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{iurl('/')}}"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">My Announcement</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row mt-40">
    <div class="col-md-12">
        <div class="container">
            @if (!empty($announcments->first()))
                @foreach ($announcments as $announcment)  
                <div class="card">                    
                    <div class="card-header bg-white">
                       <div class="col-md-11">
                           <div class="media">
                               <div class="the-photo mr-2">
                                   <img src="{{asset('student/assets/img/dummy/u3.png')}}" style="width: 50px; height: 50px" alt="">
                               </div>
                               <div class="discription p-0">
                                   <div class="dis-content pl-2 pt-2">
                                       <h6 class="m-1"> {{$announcment->user->full_name}}  <span class=" ml-50">Send to: &nbsp;{{$announcment->receive_user}}</span></h6>
                                       <small class=" ml-2">{{$announcment->created_at}}</small>
                                     
                                    </div>
                                </div>
                            </div>
                       </div>
                    
                       
                        <div class="manage pull-left">
                            <a data-toggle="tooltip" data-placement="top"   title="Edit Announcment" class="btn btn-icon btn-primary btn-add-Edit" href='{{ route('instructor.announcments.edit', $announcment->id) }}'><i class="ik ik-edit f-16 mr-10"></i></a>
                            <a data-toggle="modal" data-target="#deleteModal{{$announcment->id}}" class="btn btn-icon btn-danger btn-add-Delete" href='#'><i class="ik ik-trash-2 f-16 mr-10" data-toggle="tooltip" data-placement="top" title="Delete Announcment" ></i></a>
                        </div>
                       
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-12 col-sm-6 col-lg-11 offset-md-1">
                                <div class="row">
                                    
                                </div>
                                <div class="Question">
                                    {!! $announcment->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="modal fade" id="deleteModal{{$announcment->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel">Delete Lecture</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <form action="{{ route('instructor.announcments.delete', $announcment->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <div class="modal-body">
                                    Do you want delete this announcment?
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" id="deleteVstudy" value="Delete" class="btn btn-danger"/>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
            @else
            <div class="text-center p-5">
                <br><br><br><br>
                <i class="icon-note-important s-64 text-primary"></i>
                <h3 class="my-3">No Announcment Found</h3>
                <p>There are no Announcment from you</p>
                <a href="{{iurl('announcments/myAnnouncments')}}" class="btn btn-primary shadow btn-lg"><i class="icon-recycle mr-2 "></i>Refresh Again</a>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
