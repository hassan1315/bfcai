@extends('layouts.Instructor.app')
@push('script')
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('editor1', {
                extraPlugins: 'placeholder'
                , height: 200,
                language: 'en'
            });
        });
    </script>
    <script> $(document).ready(function() { $('.select1').niceSelect(); }); </script>
@endpush
@section('content')

<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-send bg-blue"></i>
                <div class="d-inline">
                    <h2>Announcement</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{iurl('/')}}"><i class="ik ik-home"></i></a>
                    </li>

                    <li class="breadcrumb-item active" aria-current="page">Announcement</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Customer overview start -->
<div class="row mt-35">
    <div class="col-md-12">
        <div class="card my-4">
            <h5 class="card-header">Edit Announcement</h5>
            <div class="card-body">
                <form action="{{route('instructor.announcments.update',$announcment->id)}}" method="post">
                    @csrf
                    <div class="form-group">
                        <textarea class="form-control" required cols="10" id="editor1" name="content" rows="15">{{ $announcment->content }}</textarea>
                    </div>
                    <div class="anno">
                        <h6>&nbsp; &nbsp; Send Announcement to ?</h6>
                        <div class="form-group col-md-4">
                            <select name="receive_user" class="select1 wide" style="width: 100%" class="form-control">
                                <option value="students" @if($announcment->receive_user == 'students') selected @endif>Students</option>
                                <option value="instructors" @if($announcment->receive_user == 'instructors') selected @endif>Instructors</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary col-md-2" style="margin-left: 39%; margin-top: -50px;">Send <label class="ik ik-send"></label></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
