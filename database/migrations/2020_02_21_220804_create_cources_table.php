<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('cource_code')->nullable();
            $table->string('image')->default('instructors/cources/photo/cource.png');
            $table->text('description')->nullable();
            $table->string('share_doctor_id')->nullable();
            $table->string('share_asstisant_id')->nullable();
            $table->enum('semester', ['first-semester', 'second-semester'])->default('first-semester');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('instructors')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cources');
    }
}
