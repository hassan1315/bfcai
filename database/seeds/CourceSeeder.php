<?php

use App\Cource;
use Illuminate\Database\Seeder;

class CourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cource::create([
            'name_ar' => 'الشبكات',
            'name_en' => 'Network',
            'cource_code' => 'IS-N42',
            'semester' => 'first-semester',
            'image' => 'instructors/cources/photo/cource.png',
            'description' => 'This Is Test Description',
            'user_id' => 1,
        ]);
        Cource::create([
            'name_ar' => 'اساليب الامان',
            'name_en' => 'Security',
            'cource_code' => 'IS-S120',
            'semester' => 'second-semester',
            'image' => 'instructors/cources/photo/cource.png',
            'description' => 'This Is Test Description',
            'user_id' => 1,
        ]);
    }
}
