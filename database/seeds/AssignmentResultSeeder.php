<?php

use App\AssignmentResult;
use Illuminate\Database\Seeder;

class AssignmentResultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssignmentResult::create([
            'name' => 'Assignemt 1',
            'description' => 'This Is Test Description',
            'assignment_id'=> 1,
            'content' => 'students/lecture/assignment/sheet sol 1.pdf',
            'type' => 'lecture',
            'user_id' => 1
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 1',
            'description' => 'This Is Test Description',
            'assignment_id'=> 1,
            'content' => 'students/lecture/assignment/sheet sol 1.pdf',
            'type' => 'section',
            'user_id' => 1
        ]);
    
        AssignmentResult::create([
            'name' => 'Assignemt 1',
            'content' => 'students/lecture/assignment/sheet sol 1.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 1,
            'type' => 'lecture',
            'user_id' => 2
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 1',
            'content' => 'students/section/assignment/sheet sol 1.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 1,
            'type' => 'section',
            'user_id' => 2
        ]);
    
        AssignmentResult::create([
            'name' => 'Assignemt 1',
            'content' => 'students/lecture/assignment/sheet sol 1.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 1,
            'type' => 'lecture',
            'user_id' => 3
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 1',
            'content' => 'students/section/assignment/sheet sol 1.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 1,
            'type' => 'section',
            'user_id' => 3
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 2',
            'description' => 'This Is Test Description',
            'assignment_id'=> 2,
            'content' => 'students/lecture/assignment/sheet sol 2.pdf',
            'type' => 'lecture',
            'user_id' => 1
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 2',
            'description' => 'This Is Test Description',
            'assignment_id'=> 2,
            'content' => 'students/lecture/assignment/sheet sol 2.pdf',
            'type' => 'section',
            'user_id' => 1
        ]);
    
        AssignmentResult::create([
            'name' => 'Assignemt 2',
            'content' => 'students/lecture/assignment/sheet sol 2.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 2,
            'type' => 'lecture',
            'user_id' => 2
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 2',
            'content' => 'students/section/assignment/sheet sol 2.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 2,
            'type' => 'section',
            'user_id' => 2
        ]);
    
        AssignmentResult::create([
            'name' => 'Assignemt 2',
            'content' => 'students/lecture/assignment/sheet sol 2.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 2,
            'type' => 'lecture',
            'user_id' => 3
        ]);

        AssignmentResult::create([
            'name' => 'Assignemt 2',
            'content' => 'students/section/assignment/sheet sol 2.pdf',
            'description' => 'This Is Test Description',
            'assignment_id'=> 2,
            'type' => 'section',
            'user_id' => 3
        ]);
    
    }
}
