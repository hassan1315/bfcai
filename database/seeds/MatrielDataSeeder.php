<?php

use App\MatrielData;
use Illuminate\Database\Seeder;

class MatrielDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MatrielData::create([
            'name' => 'Chapter 1',
            'type' => 'document',
            'description' => 'This Is Test Description',
            'matriel_id' => 1,
        ]);
        MatrielData::create([
            'name' => 'Desstion Tree',
            'type' => 'video',
            'description' => 'This Is Test Description',
            'matriel_id' => 1,
        ]);
        MatrielData::create([
            'name' => 'Section 1',
            'type' => 'document',
            'description' => 'This Is Test Description',
            'matriel_id' => 2,
        ]);
        MatrielData::create([
            'name' => 'Weka',
            'type' => 'video',
            'description' => 'This Is Test Description',
            'matriel_id' => 2,
        ]);
        MatrielData::create([
            'name' => 'Chapter 2',
            'type' => 'document',
            'description' => 'This Is Test Description',
            'matriel_id' => 3,
        ]);
        MatrielData::create([
            'name' => 'K Mean',
            'type' => 'video',
            'description' => 'This Is Test Description',
            'matriel_id' => 3,
        ]);
        MatrielData::create([
            'name' => 'Section 2',
            'type' => 'document',
            'description' => 'This Is Test Description',
            'matriel_id' => 4,
        ]);
        MatrielData::create([
            'name' => 'Python',
            'type' => 'video',
            'description' => 'This Is Test Description',
            'matriel_id' => 4,
        ]);
    }
}
